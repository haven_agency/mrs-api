<?php

namespace Application\Utility;

require_once('./SesMetrics.php');
require_once('./SesSend.php');
require_once('./EmailTemplate.php');

$metrics = new SesMetrics();
$mailer = new SesSend();

$total_sent = $metrics->getSent(2, 'notify' , null, '2018-01-01','2019-01-01');
echo 'Total Sends: ' . $total_sent . PHP_EOL;

$opens = $metrics->getOpens(2, 'notify' , '2018-01-01','2019-01-01');
echo 'Total Opens: ' . $opens['total_opens'] . PHP_EOL;
echo 'Total Unique Opens: ' . $opens['total_unique'] . PHP_EOL;

$clicks = $metrics->getClicks(2, 'notify' , '2018-01-01','2019-01-01');
echo 'Total Clicks: ' . $clicks['total_clicks'] . PHP_EOL;
echo 'Total Unique Clicks: ' . $clicks['total_unique'] . PHP_EOL;

$html = new EmailTemplate("email-notify"); 
$html->set([
    'emailHeaderImage'     => 'test.jpg',
    'emailBackgroundColor' => '#CC2211',
    'emailFontColor'       => '#112233',
    'emailButtonColor'     => '#000000',
    'emailButtonFontColor' => '#CCCCCC',
    'emailButtonText'      => 'GET IT',
    'emailStyle'           => '',
    'emailBody1'           => 'this is a test email sent for testing...and stuff.',
    'emailBody2'           => 'style and stuff',
    'emailStyleColor'      => '#dad6d5',
    'header_logo'          =>  '123',
    'url'			       => 'https://google.com'
]);

$html = $html->render();

$mailer->sendNotificationEmail('armando@havenagency.com', 'ARMANDO - TEST API', $html);