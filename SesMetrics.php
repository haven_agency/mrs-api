<?php
namespace Application\Utility;

use PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Application\Utility\SESCurl;

class SesMetrics
{
  public $response;

  public function __construct($account=false)
  {
      // $this->curl =  new Curl($api_kxey);
      $this->SESCurl = new SESCurl();
  }

  /*
  *  Get Sent data
  */
  public function getSent($campaign_id, $campaign_type, $reminder_id=null, $range_from=null, $range_to=null) 
  {
    $payload = $this->setPayload($campaign_id, $campaign_type);
    $response = $this->SESCurl->post('sends', '_search', $payload);

    $obj = json_decode($response); 
    return $obj->hits->total;
  }

  /*
  *  Get Open data
  */
  public function getOpens($campaign_id, $campaign_type, $range_from=null, $range_to=null) 
  {
    $payload = $this->setPayload($campaign_id, $campaign_type);
    $response = $this->SESCurl->post('opens', '_search', $payload);

    $obj = json_decode($response);
    return ['total_opens' => $obj->hits->total, 'total_unique' => $obj->aggregations->unique->value];
  }

  /*
  *  Get Click data
  */
  public function getClicks($campaign_id, $campaign_type, $range_from=null, $range_to=null)
  {
    $payload = $this->setPayload($campaign_id, $campaign_type);
    $response = $this->SESCurl->post('clicks', '_search', $payload);

    $obj = json_decode($response);
    return ['total_clicks' => $obj->hits->total, 'total_unique' => $obj->aggregations->unique->value];
  }

    /*
  *  Get Bounce data
  */
  public function getBounces($campaign_id, $campaign_type)
  {
    return ['total_clicks'=>0, 'unique_clicks'=>0];
  }

  /*
  *  Get Sent Click, Open metrics for a campaign
  */
  public function getStats($campaign_id, $campaign_type, $campaign_slug=null) 
  {

    // CMS will call it like:
    // {bundle_id}_{type}_{reminder_id}
    // $SesMetrics->getStats('50271_reminder_31');

    //TODO combine all metrics into one result set
    return [
      'total_sents' => $this->getSent($campaign_id, $campaign_type),
      'total_opens' => $this->getOpens($campaign_id, $campaign_type),
      'total_clicks' => $this->getClicks($campaign_id, $campaign_type)
    ];
  }

  //TODO: Add custom filters that can be passed on to payload.
  public function setPayload($campaign_id, $campaign_type)
  {
    $payload = [
      'size' => 0,
      'query' => [
        'bool' => [
          'must' => [
            [ 'match' => [ 'mail.tags.CAMPAIGN_ID' => $campaign_id ] ],
            [ 'match' => ['mail.tags.CAMPAIGN_TYPE' => $campaign_type ] ],
          ]
        ]
      ],
      'aggs' => [
        'unique' => [
          'cardinality' => [
            'field' => 'mail.messageId.keyword'
          ]
        ]
      ]
    ];

    return json_encode($payload);
  }
}

