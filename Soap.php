<?php

namespace Application\Utility;

class Soap
{
    public $soap;
    private $authentication;
    public $response;
    public $errors;

    public function __construct($url, $soap_action) 
    {
        $env = getenv('APP_ENV') ? : 'development';
        $this->soap = new \SoapClient($url.$soap_action, array('trace' => true));
    }

    // Returns a SoapVar containing authentication creds
    public function getAuthSoapVar($type, $header)
    {
        $env = getenv('APP_ENV') ? : 'development';
        $username = $env == 'development' ? 'dev@havenagency.com' : '';
        $password = $env == 'development' ? 'fishfase1!' : '';

        $auth = new \SoapVar(array(
            "Username" => $username,
            "Password" => $password
        ), SOAP_ENC_OBJECT, $type, $header);

        return $auth;
    }

    // Returns a list of usable functions
    public function getFunctions()
    {
        return $this->soap->__getFunctions();
    }

    // Returns a list of usable types;
    public function getTypes()
    {
        return $this->soap->__getTypes();
    }

    // Returns last posted SOAP Request
    public function getLastRequest()
    {
        return $this->soap->__getLastRequest();
    }

}