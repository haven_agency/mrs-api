<?php

namespace Application\Utility;

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;
use PHPMailer\PHPMailer\PHPMailer;

class SesSend
{
	public function __construct()
	{

		try {

			$credentials = null;
			if ($_ENV['APP_ENV'] == 'local') {
				$credentials = [
					'key' => $_ENV['AWS_KEY'],
					'secret' => $_ENV['AWS_SECRET']
				];
			}

			$this->mailer = SesClient::factory([
				// 'profile' => 'default',
				'version' => '2010-12-01',
				'region'  => $_ENV['AWS_REGION'] ?: 'us-west-2',
				'credentials' => $credentials
			]);
		} catch (AwsException $e) {
			// output error message if fails
			echo $e->getMessage();
			echo ("The email was not sent. Error message: " . $e->getAwsErrorMessage() . "\n");
			echo "\n";
		}
	}

	public function buildMailTemplate($type, $bundle, $customer)
	{
		$html_template = '';
		$html_body = '';
		$subject = '';

		// Set email template file based on type of mail to be sent.
		// Set email body based on type.
		switch ($type) {
			case 'notify':
				$html_template = 'email-notify';
				$subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['notifySubject']);
				$html_body = str_replace("[~first_name~]", $customer['first_name'], $bundle['notifyBody1']);
				break;
			case 'swaf':
				$html_template = 'email-swaf';
				$subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailShareSubject']);
				$html_body = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailShareBody1']);
				break;
			case 'thirdparty_primary':
				$html_template = 'email';
				$subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailSubject']);
				$html_body = str_replace("%REDEMPTIONCODES%", $bundle['codes'], $bundle['emailBody1']);
				break;
			case 'primary':
				$html_template = 'email';
				$subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailSubject']);
				$html_body = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailBody1']);
				$html_body = str_replace("%REDEMPTIONCODES%", $bundle['codes'], $html_body);
				break;
			default:
				$html_template = 'email';
				$subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailSubject']);
				$html_body = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailBody1']);
				break;
		}

		$html = new EmailTemplate($html_template);
		$html->set([
			'emailHeaderImage' => $bundle['emailHeaderImage'],
			'emailBackgroundColor' => $bundle['emailBackgroundColor'],
			'emailFontColor' => $bundle['emailFontColor'],
			'emailButtonColor' => $bundle['emailButtonColor'],
			'emailButtonFontColor' => $bundle['emailButtonFontColor'],
			'emailButtonText' => $bundle['emailButtonText'],
			'emailStyle' => $bundle['emailStyle'],
			'emailBody1' => $html_body,
			'emailBody2' => $bundle['emailBody2'],
			'emailStyleColor' => ($bundle['emailStyle'] == 'dark') ? '#25292a' : '#dad6d5',
			'header_logo' => $bundle['headerLogoID'],
			'url' => $customer['url'],
			'first_name' => isset($customer['first_name']) ? $customer['first_name'] : null
		]);
		$html = $html->render();

		return ['subject' => $subject, 'body' => $html];
	}

	public function sendNotificationEmail($email, $bundle_id, $type, $subject, $html)
	{
		try {
			$sender_email = 'Ticketmaster <redeem@email.bundles.ticketmaster.com>';
			$configuration_set = 'MRS';
			$char_set = 'UTF-8';


			// Create a new PHPMailer object.
			$mail = new PHPMailer;

			// Add components to the email.
			$mail->setFrom('redeem@email.bundles.ticketmaster.com', 'Ticketmaster');
			$mail->addAddress($email);
			$mail->Subject = $html['subject'];
			$mail->Body = $html['body'];
			$mail->AltBody = 'Please use a compatitable html email viewer.';
			$mail->CharSet = 'UTF-8';

			// Attempt to assemble the above components into a MIME message.
			if (!$mail->preSend()) {
				echo $mail->ErrorInfo;
			} else {
				// Create a new variable that contains the MIME message.
				$message = base64_encode($mail->getSentMIMEMessage());
			}


			$result = $this->mailer->sendRawEmail([
				'Destinations' => [
					$email,
				],
				'Source' => 'redeem@email.bundles.ticketmaster.com',
				'RawMessage' => [
					'Data' => $message
				],
				'ConfigurationSetName' => $configuration_set,
				'Tags' => [
					[
						'Name' => 'CAMPAIGN_ID',
						'Value' => $bundle_id,
					],
					[
						'Name' => 'CAMPAIGN_TYPE',
						'Value' => $type,
					],
				]
			]);
			$messageId = $result['MessageId'];
			// echo("Email sent! Message ID: $messageId"."\n");
		} catch (AwsException $e) {
			// output error message if fails
			// echo $e->getMessage();
			// echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
			// echo "\n";
			error_log($e->getMessage());
			error_log($e->getAwsErrorMessage());
		}
	}
}
