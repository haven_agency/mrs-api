<?php
namespace Application\Utility;

class SESCurl
{

    public function __construct()
    {
        $header = array(
            'Content-Type: application/json',
            'cache-control: no-cache',
        );

        $this->curl = curl_init();
        $this->curl_url = 'https://search-mrs-metrics-kko6j65xj5xjenf5uyyy6lfegu.us-west-2.es.amazonaws.com/';
        
        curl_setopt_array($this->curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => $header,
          ));
    }

    public function post($index, $action, $payload)
    {
        curl_setopt($this->curl, CURLOPT_URL, $this->curl_url.$index.'/'.$action); 
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $payload);
        
        $this->response = $this->exec();

        return $this->response;
    }

    public function exec()
    {   
        $response = curl_exec($this->curl);

        if (curl_error($this->curl))
        {
            $this->errors = curl_error($this->curl);
        }
        
        return $response;
    }
}