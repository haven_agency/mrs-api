<?php

namespace Application\Utility;

use Application\Utility\Campaigner;

class createCampaignNotificationTest extends \PHPUnit_Framework_TestCase
{
    public function setUp() { } 
    public function tearDown() { }

    public function __construct()
    {
        $this->campaigner = new Campaigner();
    }
    
    // Creates campaign on Campaigner Staging
    public function testCreateCampaign()
    {
        $test_obj = $this->campaigner->create_notification_campaign();
        $this->assertGreaterThan(0, $test_obj->CreateUpdateCampaignResult);

        print "\nCampaign Successfully Created.";
        print "\nCampaign ID: ".$test_obj->CreateUpdateCampaignResult;
    
    }

    // Sets notify group to campaign
    public function testsetCampaignRecipients()
    {
        $test_obj = $this->campaigner->set_campaign_recipients(23790731, 12444295);
        
        print "\nRecipients added.";
        print_r($test_obj);
    }

    // schedules camppaign
    public function testScheduleCampaign()
    {
        $test_obj = $this->campaigner->schedule_campaign(23790666);

        print "\nCampaign Scheduled";
    }
}