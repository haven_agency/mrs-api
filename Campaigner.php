<?php

namespace Application\Utility;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Application\Utility\EmailTemplate;
use Application\Utility\Curl;
use Application\Utility\Soap;

class Campaigner
{
  public $response;
  public $errors;
  public $curl;
  public $soap;
  protected $api_base_url = 'https://edapi.campaigner.com/v1';
  protected $soap_base_url = 'https://ws.campaigner.com/2013/01/';
  const PREVIEW_URL = "http://tm-mrs-staging.havenagencyapps.com";

  public function __construct($account=false)
  {
      if ($account=="thirdparty") {
          $this->soap_user = CAMPAIGNER_SOAP_USER_3RD_PARTY;
          $this->soap_pass = CAMPAIGNER_SOAP_PASS_3RD_PARTY;
          $this->smtp_user = CAMPAIGNER_SMTP_USER_3RD_PARTY;
          $this->smtp_pass = CAMPAIGNER_SMTP_PASS_3RD_PARTY;
          $api_key = CAMPAIGNER_API_KEY_THIRD_PARTY;
      }else {
          $this->soap_user = CAMPAIGNER_SOAP_USER;
          $this->soap_pass = CAMPAIGNER_SOAP_PASS;
          $this->smtp_user = CAMPAIGNER_SMTP_USER;
          $this->smtp_pass = CAMPAIGNER_SMTP_PASS;
          $api_key = CAMPAIGNER_API_KEY;
      }
      $this->curl =  new Curl($api_key);
  }

  /*
  * DEL SMTP GROUP
  */
  public function delete_smtp_group($id)
  {

    $curl = $this->curl;
    $curl->del($api_base_url."/RelaySends/$id");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }
    return $curl->to_json();
  }

  /*
  * GET SMTP GROUPS
  */
  public function get_smtp_groups()
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get($api_base_url."/RelaySends");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }
    return $curl->to_json();
  }

  /*
  *  GET SMTP STATS
  */
  public function get_smtp_stats($campaignerGroupId)
  {
    $url = 'https://ws.csmtp.net/2014/06/SMTPService.asmx?WSDL';
    $client = new \SoapClient($url, array('trace' => true));

      $params = array(
          "authentication" =>array(
            "Username" => $this->smtp_user,
            "Password" => $this->smtp_pass
          ),
          "groupByDomain" => false,
          "campaignFilterSmtp" => array(
            "ReportGroupIds" => array("int" => $campaignerGroupId)
          ),
      );

    try {
      $res_obj = $client->GetSmtpReportGroupSummary($params);
      $response = $client->__getLastRequest();
      return $res_obj;
    } catch(SOAPFault $e) {
      return $e;
    }
  }

  /* 
   * SMTP STATS ARRAY
   */
  public function get_smtp_stats_array($campaignerGroupId) 
  {
    $email_stats = array(
      'total_sent'=>0,
      'total_opens'=>0,
      'total_clicks'=>0,
    );

    $stats = $this->get_smtp_stats($campaignerGroupId);
    if (isset($stats)) {
      $c_runs = @$stats->GetSmtpReportGroupSummaryResult->ReportGroups->ReportGroup->CampaignRuns->CampaignRun;

      if (isset($c_runs)) {
        if (is_array($c_runs)) {
          foreach ($c_runs as $c_run) {
            $dresults = @$c_run->Domains->Domain->DeliveryResults;
            $aresults = @$c_run->Domains->Domain->ActivityResults;

            $email_stats['total_sent'] += @$dresults->Sent;
            $email_stats['total_opens'] += @$aresults->Opens;
            $email_stats['total_clicks'] += @$aresults->Clicks;
          }
        } else {
          $dresults = @$c_runs->Domains->Domain->DeliveryResults;
          $aresults = @$c_runs->Domains->Domain->ActivityResults;

          $email_stats['total_sent'] += @$dresults->Sent;
          $email_stats['total_opens'] += @$aresults->Opens;
          $email_stats['total_clicks'] += @$aresults->Clicks;
        }
      }
    }

    return $email_stats;
  }

/*
  * Generate a new campaign and schedule send.
  * NOTE: $bundle is expected to be an Entity Object  
  */
  public function create_notification_campaign($bundle, $notifytime, $notifySubject) 
  {
    if(empty($bundle))
    {
      return "missing bundle";
    }

    $html = new EmailTemplate('email-notify');
    $html->set([      
      'emailHeaderImage'=> $bundle->getEmailHeaderImage(),
      'emailBackgroundColor'=> $bundle->getEmailBackgroundColor(),
      'emailFontColor'=> $bundle->getEmailFontColor(),
      'emailButtonColor'=> $bundle->getEmailButtonColor(),
      'emailButtonFontColor'=> $bundle->getEmailButtonFontColor(),
      'emailButtonText'=> $bundle->getEmailButtonText(),
      'emailStyle'=> $bundle->getEmailStyle(),
      'emailBody1'=>$bundle->getNotifyBody1(),
      'emailBody2'=>$bundle->getEmailBody2(),
      'emailStyleColor'=> ($bundle->getEmailStyle()=='dark') ? '#25292a' : '#dad6d5',
      'header_logo'=>$bundle->getHeaderLogoID(),
    ]);
    //$html->render();

    $url = 'https://ws.campaigner.com/2012/07/campaignmanagement.asmx?WSDL';
    $client = new \SoapClient($url, array('trace' => true));

    $params = array(
      "authentication" => new \SoapVar(array(
          "Username" => $this->soap_user,
          "Password" => $this->soap_pass
      ), SOAP_ENC_OBJECT, "Authentication", "https://ws.campaigner.com/2012/07"),
      "campaignData" => array(
        "CampaignName" => $bundle->getId(). " : Notify : ".$bundle->getName(),
        "CampaignSubject" => $notifySubject,
        "CampaignFormat" => "HTML",
        "CampaignStatus" => "Scheduled",
        "HtmlContent" => $html->render(),
        "FromName" => "Ticketmaster",
        "FromEmailId" => CAMPAIGNER_FROM_EMAIL_ID,
        "ReplyEmailId"=> CAMPAIGNER_FROM_EMAIL_ID,
        "TrackReplies" => false,
        "IsWelcomeCampaign" => false,
        "Encoding" => "Western_ISO_8859_15"
      )
    );

    try{
      $res_obj = $client->CreateUpdateCampaign($params);
      $response = $client->__getLastRequest();

      // After camapaign creation; add notification list.
      $this->set_campaign_recipients($res_obj->CreateUpdateCampaignResult, $bundle->getCampaignerNotifyListId());
      
      // Schedule campaign
      $this->schedule_campaign($res_obj->CreateUpdateCampaignResult, $notifytime);
      return $res_obj;
    }
    catch(SOAPFault $e){
        return $e;
    }

  }

  /*
  * Add contact lists to Campaign
  */
  public function set_campaign_recipients($campaignId, $campaignerNotifyId)
  {
    $url = 'https://ws.campaigner.com/2012/07/campaignmanagement.asmx?WSDL';
    $client = new \SoapClient($url, array('trace' => true));

    $params = array(
      "authentication" => new \SoapVar(array(
        "Username" => $this->soap_user,
        "Password" => $this->soap_pass
      ), SOAP_ENC_OBJECT, "Authentication", "https://ws.campaigner.com/2012/07"),
      "campaignId" => $campaignId,
      "campaignRecipients" => array(
        "SendToAllContacts" => false,
        "ContactGroupIds" => array(
          "int" => $campaignerNotifyId
        )
      )
    );

    try {
      $res_obj = $client->SetCampaignRecipients($params);
      $response = $client->__getLastRequest();

      return $res_obj;
    }
    catch(SOAPFAULT $e) {
      return $e;
    }
  }

  public function schedule_campaign($campaignId, $notifytime)
  {
    $url = 'https://ws.campaigner.com/2012/07/campaignmanagement.asmx?WSDL';
    $client = new \SoapClient($url, array('trace' => true));

    $date = new \DateTime($notifytime, new \DateTimeZone('America/Los_Angeles'));
    $campaign_time = $date->setTimeZone(new \DateTimeZone('UTC'));
    //$start_date = ($date->getTimestamp()+$campaign_time->getOffset());

    $params = array(
      "authentication" => new \SoapVar(array(
        "Username" => $this->soap_user,
        "Password" => $this->soap_pass
      ), SOAP_ENC_OBJECT, "Authentication", "https://ws.campaigner.com/2012/07"),
      "campaignId" => $campaignId,
      "sendNow" => false,
      "campaignSchedule" => array(
        "StartDateUTC" =>  $campaign_time->format('Y-m-d\TH:i:s'), //gmdate('Y-m-d\TH:i:s', $start_date),
        "RecurrenceType" => "None",
        "OccurrenceCount" => 1
      )
    );

    try {
      $res_obj = $client->ScheduleCampaign($params);
      $response = $client->__getLastRequest();
      return $res_obj;
    }
    catch(SOAPFault $e) {
      return $e;
    }

  }
    
  /*
  * RESEND SHARE EMAIL
  */
  public function send_share_email($bundle, $campaignerId, $code, $customer, $type="primary")
  {
    if (empty($customer)) {
      return "missing customers";
    }

    if (empty($bundle)) {
      return "missing bundle";
    }
    
    switch ($type) {
      case "primary": default:
        if (empty($bundle['campaignerGroupId'])) {
          $group_id = $campaignerId;
          break;
        }
        $group_id = $bundle['campaignerGroupId'];
      break;
      break;
        case "reminder":
            if (empty($bundle['campaignerReminderGroupId'])) {
              return "missing bundle campaignerReminderGroupId";
            }
            $group_id = $bundle['campaignerReminderGroupId'];
        break;
    }

    #
    # Build email template
    #
    
    $subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailShareSubject']);
    $emailShareBody1 = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailShareBody1']);
  
    $html = new EmailTemplate("email-swaf"); 
    $html->set([
      'emailHeaderImage'=> $bundle['emailHeaderImage'],
      'emailBackgroundColor'=> $bundle['emailBackgroundColor'],
      'emailFontColor'=> $bundle['emailFontColor'],
      'emailButtonColor'=> $bundle['emailButtonColor'],
      'emailButtonFontColor'=> $bundle['emailButtonFontColor'],
      'emailButtonText'=> $bundle['emailButtonText'],
      'emailStyle'=> $bundle['emailStyle'],
      'emailBody1'=>$emailShareBody1,
      'emailBody2'=>$bundle['emailBody2'],
      'emailStyleColor'=> ($bundle['emailStyle']=='dark') ? '#25292a' : '#dad6d5',
      'header_logo'=>$bundle['headerLogoID'],
    ]);
    $html = $html->render();
    $preview_url = defined('PREVIEW_URL') ? PREVIEW_URL : 'http://tm-mrs-staging.havenagencyapps.com';
    $payload = '{
			"Recipients": [
				{
				"ToEmail": "'.$code['shareEmail'].'",
				"Variables": [
					{
					"VarName": "$url$",
					"VarValue": "'.$preview_url."/{$code['bundle_id']}/{$code['code']}".'"
					}
				]
				}
			],
			"Subject": "'.$subject.'",
			"HTML": '.json_encode($html).'
    }';
    
    $curl = $this->curl;
    $curl->post($this->api_base_url.'/RelaySends/'.$group_id, $payload);
    
    $res_obj = $curl->to_json();
    return $res_obj;

  }

  /*
  * RESEND PRIMARY SINGLE EMAIL
  */
  public function send_single_email($bundle, $campaignerId, $customer, $type="primary")
  {
    if (empty($customer)) {
      return "missing customers";
    }

    if (empty($bundle)) {
      return "missing bundle";
    }
    
    switch ($type) {
      case "primary": default:
          if (empty($bundle['campaignerGroupId'])) {
            $group_id = $campaignerId;
            break;
          }
          $group_id = $bundle['campaignerGroupId'];
      break;
      case "reminder":
          if (empty($bundle['campaignerReminderGroupId'])) {
            return "missing bundle campaignerReminderGroupId";
          }
          $group_id = $bundle['campaignerReminderGroupId'];
      break;
  }

    #
    # Build email template
    #
    
    $subject = str_replace("[~first_name~]", $customer['first_name'], $bundle['emailSubject']);
    $emailBody1 = str_replace("%REDEMPTIONCODES%",$bundle['codes'],$bundle['emailBody1']);
    
    $html = new EmailTemplate("email"); 
    $html->set([
      'emailHeaderImage'=> $bundle['emailHeaderImage'],
      'emailBackgroundColor'=> $bundle['emailBackgroundColor'],
      'emailFontColor'=> $bundle['emailFontColor'],
      'emailButtonColor'=> $bundle['emailButtonColor'],
      'emailButtonFontColor'=> $bundle['emailButtonFontColor'],
      'emailButtonText'=> $bundle['emailButtonText'],
      'emailStyle'=> $bundle['emailStyle'],
      'emailBody1'=>$emailBody1,
      'emailBody2'=>$bundle['emailBody2'],
      'emailStyleColor'=> ($bundle['emailStyle']=='dark') ? '#25292a' : '#dad6d5',
      'header_logo'=>$bundle['headerLogoID'],
    ]);
    $html = $html->render();

    $payload = '{
			"Recipients": [
				{
				"ToEmail": "'.$customer['email'].'",
				"Variables": [
          {
            "VarName": "$first_name$",
            "VarValue": "'.$customer['first_name'].'"
          },
					{
					"VarName": "$url$",
          "VarValue": "'.$customer['url'].'"
					}
				]
				}
			],
			"Subject": "'.$subject.'",
			"HTML": '.json_encode($html).'
    }';
    
    $curl = $this->curl;
    $curl->post($this->api_base_url.'/RelaySends/'.$group_id, $payload);
    
    $res_obj = $curl->to_json();
    return $res_obj;

  }

    /*
  * RESEND SINGLE THIRD PARTY EMAIL
  */
  public function send_single_third_party_email($campaign, $campaignerId,$customer, $type="primary")
  {
    if (empty($customer)) {
      return "missing customers";
    }

    if (empty($campaign)) {
      return "missing campaign";
    }
    
    switch ($type) {
      case "primary": default:
        if (empty($bundle['campaignerGroupId'])) {
          $group_id = $campaignerId;
          break;
        }
        $group_id = $campaign['campaignerGroupId'];
      break;
        case "reminder":
            if (empty($campaign['campaignerReminderGroupId'])) {
              return "missing bundle campaignerReminderGroupId";
            }
            $group_id = $campaign['campaignerReminderGroupId'];
        break;
    }

    #
    # Build email template
    #
    
    $subject = str_replace("[~first_name~]", $customer['first_name'], $campaign['emailSubject']);
    $emailBody1 = str_replace("%REDEMPTIONCODES%",$campaign['codes'],$campaign['emailBody1']);
    
    $html = new EmailTemplate("email"); 
    $html->set([
      'emailHeaderImage'=> $campaign['emailHeaderImage'],
      'emailBackgroundColor'=> $campaign['emailBackgroundColor'],
      'emailFontColor'=> $campaign['emailFontColor'],
      'emailButtonColor'=> $campaign['emailButtonColor'],
      'emailButtonFontColor'=> $campaign['emailButtonFontColor'],
      'emailButtonText'=> $campaign['emailButtonText'],
      'emailStyle'=> $campaign['emailStyle'],
      'emailBody1'=>$emailBody1,
      'emailBody2'=>$campaign['emailBody2'],
      'emailStyleColor'=> ($campaign['emailStyle']=='dark') ? '#25292a' : '#dad6d5',
      'header_logo'=>$campaign['headerLogoID'],
    ]);
    $html = $html->render();

    $payload = '{
			"Recipients": [
				{
				"ToEmail": "'.$customer['email'].'",
				"Variables": [
          {
            "VarName": "$first_name$",
            "VarValue": "'.$customer['first_name'].'"
          },
					{
					"VarName": "$url$",
          "VarValue": "'.$customer['url'].'"
					}
				]
				}
			],
			"Subject": "'.$subject.'",
			"HTML": '.json_encode($html).'
    }';
    
    $curl = $this->curl;
    $curl->post($this->api_base_url.'/RelaySends/'.$group_id, $payload);
    
    $res_obj = $curl->to_json();
    return $res_obj;

  }

  //TODO: Add error handling.
  /*
  * CREATE MAILING LIST IN CAMPAIGNER
  */
  public function soap_create_mailing_list($bundle, $type="Notify")
  {
      $url = $this->soap_base_url.'listmanagement.asmx?WSDL';
      $client = new \SoapClient($url, array('trace' => true));

      $params = array(
        "authentication" => new \SoapVar(array(
          "Username" => $this->soap_user,
          "Password" => $this->soap_pass
        ), SOAP_ENC_OBJECT, "Authentication", "https://ws.campaigner.com/2013/01"),
        "contactGroupType" => "MailingList",
        "contactGroupId" => 0,
        "name" => $bundle['id'].' : '.$type.' : '.$bundle['name'],
        "description" => "sup",
        "isGroupVisible" => true,
        "isTempGroup" => false
      );

      try{
        $res_obj = $client->CreateUpdateContactGroups($params);
        $response = $client->__getLastRequest();
        return $res_obj;
      }
      catch(SOAPFault $e){
          print $e;
      }
  }

    /*
  *  ADD EMAIL TO CAMAPAIGNER MAILING LIST
  *  NOTE: 
  *       For adding custom attributes to a field, SOAP_LITERAL and SOAP_DOCUMENT **MUST** be defined when create a SoapClient instance.
  *       Otherwise your attributes will not be appeneded to your desired field.
  *       
  */
  public function add_email_to_mailing_list($bundle, $email, $redeem_url)
  {
    $url = $this->soap_base_url.'contactmanagement.asmx?WSDL';
    $client = new \SoapClient($url, array(
                              'trace' => true,
                              'use' => SOAP_LITERAL,
                              'style' => SOAP_DOCUMENT
                            ));
    $params = array(
      "authentication" => new \SoapVar(array(
          "Username" => $this->soap_user,
          "Password" => $this->soap_pass
      ), SOAP_ENC_OBJECT, "Authentication", "https://ws.campaigner.com/2013/01"),
      "UpdateExistingContacts" => true,
      "TriggerWorkflow" => true,
      "contacts" => array(
        "ContactData" => array(
            "ContactKey" => new \SoapVar(array(
              "ContactId" => 0,
              "ContactUniqueIdentifier" => $email
            ),SOAP_ENC_OBJECT, "ContactKey", "https://ws.campaigner.com/2013/01"),
            "EmailAddress" => $email,
            "FirstName" => NULL,
            "LastName" => NULL,
            "PhoneNumber" => NULL,
            "Status" => "Subscribed",
            "MailFormat" => "HTML",
            "IsTestContact" => false,
            "CustomAttributes" => array(
              "CustomAttribute" => array(
                '_' => $redeem_url, // Field Value
                'Id' => NOTIFY_URL_ID // Attribute for Field
              )
            ),
            "AddToGroup" => array("int" => $bundle['campaignerNotifyListId'])
        )
      )
    );

    try{
      $res_obj = $client->ImmediateUpload($params);
      $response = $client->__getLastRequest();
      return $res_obj;
    }
    catch(SOAPFault $e){
        return $e;
    }
  }

  /*
  * CREATE SMTP GROUP
  */
  public function create_smtp_group($bundle, $type="Redemption")
  {

    if (empty($bundle))
    {
      return "Missing Bundle";
    }

    $data = "\"{$bundle['id']} | {$type} | {$bundle['name']}\"";

    $curl = $this->curl;
    $curl->post("https://edapi.campaigner.com/v1/RelaySends", $data);
    
    $res_obj = $curl->to_json();

    // $q = $this->db->query("UPDATE `bundles` SET campaignerGroupId='{$res_obj->RelaySendCategoryID}' WHERE id='{$bundle['id']}'");
    // $q->execute();

    return $res_obj;
  }

  /*
  * SEND AN EMAIL
  * Typical Use: CUSTOMER SERVICE AREA - RESEND EMAIL - 
  */
  public function batch_smtp_send($customers, $bundle, $type="primary") {
    
    if (empty($customers)) {
      return "missing customers";
    }

    if (empty($bundle)) {
      return "missing bundle";
    }
    
    switch ($type) {
        case "primary": default:
            if (empty($bundle['campaignerGroupId'])) {
              return "missing bundle campaignerGroupId";
            }
            $group_id = $bundle['campaignerGroupId'];
        break;
        case "reminder":
            if (empty($bundle['campaignerReminderGroupId'])) {
              return "missing bundle campaignerReminderGroupId";
            }
            $group_id = $bundle['campaignerReminderGroupId'];
        break;
    }


    #
    # Build email template
    #
    $html = new EmailTemplate("email");
    $html->set([
        'emailHeaderImage'=> $bundle['emailHeaderImage'],
        'emailBackgroundColor'=> $bundle['emailBackgroundColor'],
        'emailFontColor'=> $bundle['emailFontColor'],
        'emailButtonColor'=> $bundle['emailButtonColor'],
        'emailButtonFontColor'=> $bundle['emailButtonFontColor'],
        'emailButtonText'=> $bundle['emailButtonText'],
        'emailStyle'=> $bundle['emailStyle'],
        'emailBody1'=>$bundle['emailBody1'],
        'emailBody2'=>$bundle['emailBody2'],
        'emailStyleColor'=> ($bundle['emailStyle']=='dark') ? '#25292a' : '#dad6d5',
        'header_logo'=>$bundle['headerLogoID'],
    ]);
    $html = $html->render();
    
    $recipients = [];
    foreach($customers as $customer) {
        $obj = ["ToEmail"=>$customer['email']];
        $obj["Variables"][] = ["VarName"=>'$first_name$', "VarValue" => $customer['first_name']];
        $obj["Variables"][] = ["VarName"=>'$url$', "VarValue" => $customer['url']];
        array_push($recipients, $obj);
    }
   
    $payload = '{
      "Recipients": '.json_encode($recipients).',
      "Subject": "'.$bundle['emailSubject'].'",
      "HTML": '.json_encode($html).'
    }';
    
    $curl = $this->curl;
    $curl->post($this->api_base_url.'/RelaySends/'.$group_id, $payload);
    
    $res_obj = $curl->to_json();
    return $res_obj;

  }

}
