<?php

namespace Application\Utility;

require_once "whatcounts.conf.php";

class WhatcountsAPI
{
  public $response;
  public $errors;
  public $curl;

  public function __construct($account=false) {
    $this->curl =  new Curl($account);
  }

    public function get_clicks($distro_id,$start_date,$end_date,$page,$page_size)
  {
        if ($distro_id) {
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Clicks.svc/$distro_id/$start_date/$end_date/$page/$page_size");
        } else {
          return [];
        }
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }
  public function get_opens($distro_id,$start_date,$end_date,$page,$page_size)
  {
      if ($distro_id) {
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Opens.svc/$distro_id/$start_date/$end_date/$page/$page_size");
      } else {
          return [];
      }
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();
  }
  /*
  * LISTS
  */
  public function create_list($bundle)
  {

    $curl = $this->curl;

    $data = '{
     "Active":true,
     "Description":"",
     "Fields":[
        {
          "Name":"id",
          "Type":"Integer"
        },
        {
          "Length":255,
          "Name":"first_name",
          "Type":"Text"
        },
        {
          "Length":255,
          "Name":"last_name",
          "Type":"Text"
        },
        {
          "Length":255,
          "Name":"event_id",
          "Type":"Text"
        },
        {
          "Name":"event_date",
          "Type":"DateTime"
        },
        {
          "Length":255,
          "Name":"confirmation_number",
          "Type":"Text"
        },
        {
          "Length":255,
          "Name":"location_name",
          "Type":"Text"
        },
        {
          "Length":1,
          "Name":"transaction_type",
          "Type":"Text"
        },
        {
          "Length":255,
          "Name":"qualifier1",
          "Type":"Text"
        },
        {
          "Length":1000,
          "Name":"header_image",
          "Type":"Text"
        },
        {
          "Length":1000,
          "Name":"url",
          "Type":"Text"
        },
        {
          "Length":255,
          "Name":"button_text",
          "Type":"Text"
        },
        {
          "Length":4000,
          "Name":"body1",
          "Type":"Text"
        },
        {
          "Length":4000,
          "Name":"body2",
          "Type":"Text"
        },
        {
          "Length":10,
          "Name":"button_color",
          "Type":"Text"
        },
        {
          "Length":10,
          "Name":"button_font_color",
          "Type":"Text"
        },
        {
          "Length":10,
          "Name":"background_color",
          "Type":"Text"
        },
        {
          "Length":10,
          "Name":"font_color",
          "Type":"Text"
        },
        {
          "Length":10,
          "Name":"crowd_background_color",
          "Type":"Text"
        },
        {
          "Length":10,
          "Name":"style",
          "Type":"Text"
        },
        {
          "Name":"sent",
          "Type":"Integer"
        },
        {
          "Name":"codes_redeemed",
          "Type":"Integer"
        },
        {
          "Length":1000,
          "Name":"reminder_body1",
          "Type":"Text"
        },
        {
          "Length":1000,
          "Name":"header_logo",
          "Type":"Text"
        }
       ],
       "Name":'.json_encode($bundle->getId().' | '.$bundle->getName()).',
       "Public":false,
       "FolderID":1,
       "ListTypeID":1,
       "PrimaryKey":"url",
       "IsPublic":false
     }';

    $curl->post('https://api7.publicaster.com/Rest/Lists.svc/', $data);
    $this->response = $curl->to_json();

    if(isset($this->response->Item->OutputErrorMessage)){
      $this->errors = $this->response->Item->OutputErrorMessage;
      return false;
    }

    return $curl->to_json()->Item->ListID;

  }
  public function create_notify_list($bundle)
  {

    $curl = $this->curl;

    $data = '{
     "Active":true,
     "Description":"",
     "Fields":[
     {
       "Length":500,
       "Name":"header_image",
       "Type":"Text"
     },
     {
       "Length":1000,
       "Name":"url",
       "Type":"Text"
     },
     {
       "Length":50,
       "Name":"button_text",
       "Type":"Text"
     },
     {
       "Length":1000,
       "Name":"body1",
       "Type":"Text"
     },
     {
       "Length":1000,
       "Name":"body2",
       "Type":"Text"
     },
     {
       "Length":10,
       "Name":"button_color",
       "Type":"Text"
     },
     {
       "Length":10,
       "Name":"button_font_color",
       "Type":"Text"
     },
     {
       "Length":10,
       "Name":"background_color",
       "Type":"Text"
     },
     {
       "Length":10,
       "Name":"font_color",
       "Type":"Text"
     },
     {
       "Length":10,
       "Name":"crowd_background_color",
       "Type":"Text"
     },
     {
       "Length":10,
       "Name":"style",
       "Type":"Text"
     }
     ],
       "Name":'.json_encode($bundle->getId().' - '.$bundle->getName()).',
       "Public":false,
       "FolderID":6,
       "ListTypeID":1,
       "PrimaryKey":"url",
       "IsPublic":false
     }';

    $curl->post('https://api7.publicaster.com/Rest/Lists.svc/', $data);
    $this->response = $curl->to_json();

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json()->Item->ListID;

  }


  /*
  * CREATE ARC SCHEDULED EMAIL SEND
  */
  public function create_arc($bundle, $start)
  {

    $start = !$start ? time() : $start;
    $curl = $this->curl;

    $data = '{
      "MailingListMappingID":'.$bundle->getListId().',
      "CreatedBy":10979,
      "ContentID":1,
      "FromAddressID":'.$bundle->getEmailSendFrom().',
      "ReplyAddressID":'.$bundle->getEmailSendFrom().',
      "CampaignID":'.$bundle->getCampaignId().',
      "GoogleAnalyticsTrackingTitle":"'.$bundle->getId().'-Redemption",
      "PhysicalMailingAddressID":'.$bundle->getPhysicalMailingAddressID().',
      "Subject":'.json_encode($bundle->getEmailSubject()).',
      "StartDate":"\/Date('.($start*1000).')\/"
    }';

    $this->data = $data;

    $curl->post('https://api7.publicaster.com/Rest/LiveNationMRS.svc/', $data);

    if ($curl->to_json()) {
      if(isset($this->response->OutputMessage) && $this->response->OutputMessage!="Success"){
        $this->errors = $this->response;
        return false;
      }
    } else {
      return false;
    }

    return $curl->response;

  }


  /*
  * DELETE ARC SCHEDULED EMAIL SEND
  */
  public function delete_arc($listId)
  {

    $curl = $this->curl;

    $this->response = $curl->del('https://api7.publicaster.com/Rest/LiveNationMRS.svc/'.$listId);

    if( $this->response != '"Success"'){
      $this->errors = "Failed to delete ARC";
      return false;
    }

    return $this->response;

  }


  /*
  * GET LISTS
  */
  public function get_lists()
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Lists.svc/");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }


  /*
  * GET LIST
  */
  public function get_list($list_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Lists.svc/$list_id");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }


  /*
  * GET LIST COUNT
  */
  public function get_list_count($list_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/ListCount.svc/$list_id");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * DEL LIST
  */
  public function delete_list($list_id)
  {

    $curl = $this->curl;
    $curl->del("https://api7.publicaster.com/Rest/Lists.svc/$list_id");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }



  /*
  * GET SEGMENTATIONS
  */
  public function get_reminder_segmentation_id_for_list($list_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Segmentation.svc/");

    $this->response = $curl->to_json();

    $segmentations = array();

    if(isset($this->response->Segmentations)){

      foreach($this->response->Segmentations as $segmentation) {
        if (($segmentation->MailingListMappingID == $list_id) && (strtolower($segmentation->SegmentationName) == 'redeemed 0 codes')) {
          $segmentations[] = $segmentation;
        }
      }

    }

    if (isset($segmentations[0])) {
      return $segmentations[0]->SegmentationID;
    }
    return false;

  }



  /*
  * GET SUBSCRIBERS
  */
  public function get_subscribers($list_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Subscribers.svc/$list_id");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * CREATE CAMPAIGNS
  */
  public function create_campaign($bundle)
  {

    $curl = $this->curl;
    $data = '{
       "EndDate":"\/Date('.((int)round(microtime(true)*1000)+500000).'-0400)\/",
       "FolderID":1,
       "Name":'.json_encode($bundle->getId().' - '.$bundle->getName()).',
       "CreatedBy":10979,
       "LastModifiedBy":10979,
       "StartDate":"\/Date('.((int)round(microtime(true)*1000)+500000).'-0400)\/"
    }';

    $curl->post('https://api7.publicaster.com/Rest/Campaigns.svc/', $data);
    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $this->response->Item->CampaignID;

  }

  /*
  * CREATE CAMPAIGNS Distro
  */

  public function create_campaign_distribution($bundle)
  {
    // 4 hours into future
    $start_date = (int)round((microtime(true)+12000)*1000);
    $curl = $this->curl;
    $data = '{
       "AdditionalLinkParams":"",
       "Cancelled":false,
       "DateScheduled":"/Date('.$start_date.'-0500)/",
       "DistributionSubject":'.json_encode($bundle->getEmailSubject()).',
       "FacebookImageUrl":"",
       "FacebookPost":"",
       "GoogleTitle":"",
       "IsSeed":false,
       "IsSegmentation":false,
       "IsSuppression":false,
       "IsTest":false,
       "LinkedInPost":"",
       "NotificationRecipients":"",
       "OmnitureCID":"",
       "PhysicalAddressID":1,
       "PONumber":"",
       "ProcessGoogle":false,
       "SelectedCampaign":'.$bundle->getCampaignId().',
       "SelectedEmail":4,
       "SelectedFromAddress":'.$bundle->getEmailSendFrom().',
       "SelectedMailingList":'.$bundle->getListId().',
       "SelectedReplyToAddress":'.$bundle->getEmailSendFrom().',
       "SelectedSeed":null,
       "SelectedSegmentation":null,
       "SelectedSuppression":null,
       "SocialEmailForReporting":"",
       "TrackLinks":true,
       "TwitterPost":"",
       "UserID":10979,
       "EnableCookielessConversions":false
    }';

    $curl->post('https://api7.publicaster.com/Rest/CampaignDistributions.svc/', $data);
    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    print_r($this->response);

    return $this->response->Item->CampaignID;

  }

  /*
  * CREATE DISTRO SEND for NOTIFY WHEN READY
  */

  public function create_notification_distribution($bundle,$subject,$notifytime,$notification_list) {

    # Transform the entered PT to ET
    $date = new \DateTime($notifytime, new \DateTimeZone('America/Los_Angeles'));
    $est_time = $date->setTimeZone(new \DateTimeZone('America/New_York'));
    $start_date = ($date->getTimestamp()+$est_time->getOffset())*1000;
    $notification_list = $notification_list ?: "jason@havenagency.com, raymond.lew@ticketmaster.com, mary.bakshiyeva@ticketmaster.com";
    
    $curl = $this->curl;
    $data = '{
       "AdditionalLinkParams":"",
       "Cancelled":false,
       "DateScheduled":"/Date('.$start_date.')/",
       "DistributionSubject":"'.$subject.'",
       "FacebookImageUrl":"",
       "FacebookPost":"",
       "GoogleTitle":"",
       "IsSeed":false,
       "IsSegmentation":false,
       "IsSuppression":false,
       "IsTest":false,
       "LinkedInPost":"",
       "NotificationRecipients":"'.$notification_list.'",
       "OmnitureCID":"",
       "PhysicalAddressID":1,
       "PONumber":"",
       "ProcessGoogle":false,
       "SelectedCampaign":'.$bundle->getCampaignId().',
       "SelectedEmail":4,
       "SelectedFromAddress":'.$bundle->getEmailSendFrom().',
       "SelectedMailingList":'.$bundle->getNotifyListId().',
       "SelectedReplyToAddress":'.$bundle->getEmailSendFrom().',
       "SelectedSeed":null,
       "SelectedSegmentation":null,
       "SelectedSuppression":null,
       "SocialEmailForReporting":"",
       "TrackLinks":true,
       "TwitterPost":"",
       "UserID":10979,
       "EnableCookielessConversions":true
    }';

    $curl->post('https://api7.publicaster.com/Rest/CampaignDistributions.svc/', $data);
    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $this->response->Item->CampaignID;

  }

  /*
  * CREATE DISTRO SEND for REMINDER EMAILS
  */

  public function create_reminder_distribution($bundle, $subject, $start_time, $notification_list) {

    # Transform the entered PT to ET
    $date = new \DateTime($start_time, new \DateTimeZone('America/Los_Angeles'));
    $est_time = $date->setTimeZone(new \DateTimeZone('America/New_York'));
    $start_date = ($date->getTimestamp() + $est_time->getOffset()) * 1000;
    $notification_list = $notification_list ?: "jason@havenagency.com, raymond.lew@ticketmaster.com, mary.bakshiyeva@ticketmaster.com";
    
    $reminder_segmentation_id = $this->get_reminder_segmentation_id_for_list($bundle->getListId());

    if (!$reminder_segmentation_id) {
      $this->errors = "Operation failed. Missing reminder_segmentation_id. The Segmentation for Reminder Emails may need to be setup on Whatcounts first.";
      return false;
    }
    $curl = $this->curl;
    $data = '{
       "AdditionalLinkParams":"",
       "Cancelled":false,
       "DateScheduled":"/Date('.$start_date.')/",
       "DistributionSubject":'.$subject.',
       "FacebookImageUrl":"",
       "FacebookPost":"",
       "GoogleTitle":"",
       "IsSeed":false,
       "IsSegmentation":true,
       "IsSuppression":false,
       "IsTest":false,
       "LinkedInPost":"",
       "NotificationRecipients":"'.$notification_list.'",
       "OmnitureCID":"",
       "PhysicalAddressID":1,
       "PONumber":"",
       "ProcessGoogle":false,
       "SelectedCampaign":'.$bundle->getCampaignId().',
       "SelectedEmail":2,
       "SelectedFromAddress":'.$bundle->getEmailSendFrom().',
       "SelectedMailingList":'.$bundle->getListId().',
       "SelectedReplyToAddress":'.$bundle->getEmailSendFrom().',
       "SelectedSeed":null,
       "SelectedSegmentation":'.$reminder_segmentation_id.',
       "SelectedSuppression":null,
       "SocialEmailForReporting":"",
       "TrackLinks":true,
       "TwitterPost":"",
       "UserID":10979,
       "EnableCookielessConversions":true
    }';

    $curl->post('https://api7.publicaster.com/Rest/CampaignDistributions.svc/', $data);
    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $this->response->Item;

  }

  /*
  * CANCEL DISTRO SEND
  */

  public function cancel_distribution($distribution_id) {

    $curl = $this->curl;

    $this->response = $curl->put('https://api7.publicaster.com/Rest/CancelCampaignDistribution.svc/'.$distribution_id);
    if($curl->response != "true"){
      $this->errors = "Did not cancel CampaignDistribution. Response from api not successful.";
      return false;
    }

    return $curl->response;

  }

  public function delete_campaign($campaign_id)
  {

    $curl = $this->curl;
    $curl->del("https://api7.publicaster.com/Rest/Campaigns.svc/$campaign_id");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * FROM ADDRESS
  */
  public function create_send_from_address($name, $email)
  {

    # Create from Address
    #
    $data = '{
       "Authorized":true,
       "CreatedByID":10979,
       "DefaultAddress":false,
       "DisplayName":"'.$name.'",
       "EmailAddress":"'.$email.'",
       "ExceptionAllowed":false,
       "LasyModifiedBy":10979
    }';
    $curl = $this->curl;
    $curl->post('https://api7.publicaster.com/Rest/FromReplyAddresses.svc/',$data);

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();
  }

  public function get_send_from_addresses()
  {
    $curl = $this->curl;
    $curl->get('https://api7.publicaster.com/Rest/FromReplyAddresses.svc/');

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();
  }

  /* PhysicalMailingAddress */

public function get_physical_mailing_addresses()
{
  $curl = $this->curl;
  $curl->get('https://api7.publicaster.com/Rest/PhysicalMailingAddresses.svc/');

  if(isset($this->response->Item->OutputMessage)){
    $this->errors = $this->response->Item->OutputMessage;
    return false;
  }

  return $curl->to_json();
}


  /*
  * GET DISTRIBUTIONS
  */
  public function get_distibutions()
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/CampaignDistributions.svc/");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET DISTRIBUTION
  */
  public function get_distibution($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/CampaignDistributions.svc/{$campaign_distribution_id}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }


  /*
  * GET DISTRIBUTION DETAILS
  */
  public function get_distibution_details($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/CampaignDistributionDetails.svc/{$campaign_distribution_id}/1/100");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET DISTRIBUTION OPENs
  */
  public function get_distibution_opens($campaign_distribution_id)
  {

    $page_size = 1000;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Opens.svc/{$campaign_distribution_id}/01012014/08012015/1/{$page_size}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET DISTRIBUTION CLICKS
  */
  public function get_distibution_clicks($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Clicks.svc/{$campaign_distribution_id}/01012014/08012015/1/{$page_size}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET DISTRIBUTION CLICKS
  */
  public function get_distibution_click_performance($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/ClickThroughPerformance.svc/{$campaign_distribution_id}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }


  /*
  * GET HARD BOUNCES
  */
  public function get_hard_bounces($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/HardBounces.svc/{$campaign_distribution_id}/01012014/08012015/1/{$page_size}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET SOFT BOUNCES
  */
  public function get_soft_bounces($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/SoftBounces.svc/{$campaign_distribution_id}/01012014/08012015/1/{$page_size}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET COMPLAINTS
  */
  public function get_complaints($campaign_distribution_id)
  {

    $page_size = 100;
    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/Complaints.svc/{$campaign_distribution_id}/01012014/08012015/1/{$page_size}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }

  /*
  * GET DISTRIBUTION STATS (CLICKS, UNIQUE CLICKS, OPENS, UNIQUE, AVG CLICK etc..)
  */
  public function get_distribution_stats($campaign_distribution_id)
  {

    $curl = $this->curl;
    $curl->get("https://api7.publicaster.com/Rest/MacroStats3.svc/{$campaign_distribution_id}");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }


  /*
  * CREATE OPTIN FORM
  */
  public function create_optin_form($bundle)
  {

    $data = '{
     "FormName":'.json_encode($bundle->getId().' - '.$bundle->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$bundle->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
         {
            "FieldValidationType":2,
            "FieldDataType":1,
            "DisplayName":"Email Address",
            "FieldName":"Email",
            "DisplayOrder":1,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"id",
            "FieldName":"id",
            "DisplayOrder":2,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"First Name",
            "FieldName":"first_name",
            "DisplayOrder":3,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Last Name",
            "FieldName":"last_name",
            "DisplayOrder":4,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event ID",
            "FieldName":"event_id",
            "DisplayOrder":5,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"event_date",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"confirmation_number",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Location Name",
            "FieldName":"location_name",
            "DisplayOrder":7,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Transaction Type",
            "FieldName":"transaction_type",
            "DisplayOrder":8,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Qualifier1",
            "FieldName":"qualifier1",
            "DisplayOrder":9,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Image",
            "FieldName":"header_image",
            "DisplayOrder":10,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"URL",
            "FieldName":"url",
            "DisplayOrder":11,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Text",
            "FieldName":"button_text",
            "DisplayOrder":12,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body1",
            "FieldName":"body1",
            "DisplayOrder":13,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body2",
            "FieldName":"body2",
            "DisplayOrder":14,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Color",
            "FieldName":"button_color",
            "DisplayOrder":15,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Font Color",
            "FieldName":"button_font_color",
            "DisplayOrder":16,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Background Color",
            "FieldName":"background_color",
            "DisplayOrder":17,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Font Color",
            "FieldName":"font_color",
            "DisplayOrder":18,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Crowd Background Color",
            "FieldName":"crowd_background_color",
            "DisplayOrder":19,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Style",
            "FieldName":"style",
            "DisplayOrder":20,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":1,
          "ConfirmationEmailSubject":'.json_encode($bundle->getEmailSubject()).',
          "ConfirmationEmailReplyAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$bundle->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$bundle->getId().'-Redemption",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    $curl->post('https://api7.publicaster.com/Rest/OptinForms.svc/', $data);

    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }




  /*
  * UPDATE OPTIN FORM
  */
  public function update_optin_form($bundle)
  {

    $data = '{
     "FormName":'.json_encode($bundle->getId().' - '.$bundle->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$bundle->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
         {
            "FieldValidationType":2,
            "FieldDataType":1,
            "DisplayName":"Email Address",
            "FieldName":"Email",
            "DisplayOrder":1,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"id",
            "FieldName":"id",
            "DisplayOrder":2,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"First Name",
            "FieldName":"first_name",
            "DisplayOrder":3,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Last Name",
            "FieldName":"last_name",
            "DisplayOrder":4,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event ID",
            "FieldName":"event_id",
            "DisplayOrder":5,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"event_date",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"confirmation_number",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Location Name",
            "FieldName":"location_name",
            "DisplayOrder":7,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Transaction Type",
            "FieldName":"transaction_type",
            "DisplayOrder":8,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Qualifier1",
            "FieldName":"qualifier1",
            "DisplayOrder":9,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Image",
            "FieldName":"header_image",
            "DisplayOrder":10,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"URL",
            "FieldName":"url",
            "DisplayOrder":11,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Text",
            "FieldName":"button_text",
            "DisplayOrder":12,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body1",
            "FieldName":"body1",
            "DisplayOrder":13,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body2",
            "FieldName":"body2",
            "DisplayOrder":14,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Color",
            "FieldName":"button_color",
            "DisplayOrder":15,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Font Color",
            "FieldName":"button_font_color",
            "DisplayOrder":16,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Background Color",
            "FieldName":"background_color",
            "DisplayOrder":17,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Font Color",
            "FieldName":"font_color",
            "DisplayOrder":18,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Crowd Background Color",
            "FieldName":"crowd_background_color",
            "DisplayOrder":19,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Style",
            "FieldName":"style",
            "DisplayOrder":20,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":1,
          "ConfirmationEmailSubject":'.json_encode($bundle->getEmailSubject()).',
          "ConfirmationEmailReplyAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$bundle->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$bundle->getId().'-Redemption",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    # get the OptinForm ID
    $pattern = '/OptinFormID=(\d+)&/';
    preg_match($pattern, $bundle->getPostUrl(), $matches);

    $curl->put('https://api7.publicaster.com/Rest/OptinForms.svc/'.$matches[1], $data);

    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }



  /*
  * CREATE OPTIN FORM
  */
  public function create_reminder_optin_form($bundle)
  {

    $data = '{
     "FormName":'.json_encode($bundle->getId().' - Reminder - '.$bundle->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$bundle->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
         {
            "FieldValidationType":2,
            "FieldDataType":1,
            "DisplayName":"Email Address",
            "FieldName":"Email",
            "DisplayOrder":1,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"id",
            "FieldName":"id",
            "DisplayOrder":2,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"First Name",
            "FieldName":"first_name",
            "DisplayOrder":3,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Last Name",
            "FieldName":"last_name",
            "DisplayOrder":4,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event ID",
            "FieldName":"event_id",
            "DisplayOrder":5,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"event_date",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"confirmation_number",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Location Name",
            "FieldName":"location_name",
            "DisplayOrder":7,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Transaction Type",
            "FieldName":"transaction_type",
            "DisplayOrder":8,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Qualifier1",
            "FieldName":"qualifier1",
            "DisplayOrder":9,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Image",
            "FieldName":"header_image",
            "DisplayOrder":10,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"URL",
            "FieldName":"url",
            "DisplayOrder":11,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Text",
            "FieldName":"button_text",
            "DisplayOrder":12,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body1",
            "FieldName":"body1",
            "DisplayOrder":13,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body2",
            "FieldName":"body2",
            "DisplayOrder":14,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Color",
            "FieldName":"button_color",
            "DisplayOrder":15,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Font Color",
            "FieldName":"button_font_color",
            "DisplayOrder":16,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Background Color",
            "FieldName":"background_color",
            "DisplayOrder":17,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Font Color",
            "FieldName":"font_color",
            "DisplayOrder":18,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Crowd Background Color",
            "FieldName":"crowd_background_color",
            "DisplayOrder":19,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Style",
            "FieldName":"style",
            "DisplayOrder":20,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Sent",
            "FieldName":"sent",
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Codes Redeemed",
            "FieldName":"codes_redeemed",
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Reminder Body1",
            "FieldName":"reminder_body1",
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":2,
          "ConfirmationEmailSubject":'.json_encode($bundle->getEmailReminderSubject()).',
          "ConfirmationEmailReplyAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$bundle->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$bundle->getId().'-Reminder",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    $curl->post('https://api7.publicaster.com/Rest/OptinForms.svc/', $data);
    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }

  /*
  * CREATE OPTIN FORM SHARE WITH A FRIEND TRANSACTION EMAILS
  */
  public function create_swaf_optin_form($bundle)
  {

    $data = '{
     "FormName":'.json_encode($bundle->getId().' - SWAF - '.$bundle->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$bundle->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
     {
        "FieldValidationType":2,
        "FieldDataType":1,
        "DisplayName":"Email Address",
        "FieldName":"Email",
        "DisplayOrder":1,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"First Name",
        "FieldName":"first_name",
        "DisplayOrder":3,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Last Name",
        "FieldName":"last_name",
        "DisplayOrder":4,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Header Image",
        "FieldName":"header_image",
        "DisplayOrder":10,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"URL",
        "FieldName":"url",
        "DisplayOrder":11,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Button Text",
        "FieldName":"button_text",
        "DisplayOrder":12,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Body1",
        "FieldName":"body1",
        "DisplayOrder":13,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Body2",
        "FieldName":"body2",
        "DisplayOrder":14,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Button Color",
        "FieldName":"button_color",
        "DisplayOrder":15,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Button Font Color",
        "FieldName":"button_font_color",
        "DisplayOrder":16,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Background Color",
        "FieldName":"background_color",
        "DisplayOrder":17,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Font Color",
        "FieldName":"font_color",
        "DisplayOrder":18,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Crowd Background Color",
        "FieldName":"crowd_background_color",
        "DisplayOrder":19,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Style",
        "FieldName":"style",
        "DisplayOrder":20,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Sent",
        "FieldName":"sent",
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Codes Redeemed",
        "FieldName":"codes_redeemed",
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Reminder Body1",
        "FieldName":"reminder_body1",
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":3,
          "ConfirmationEmailSubject":'.json_encode($bundle->getEmailShareSubject()).',
          "ConfirmationEmailReplyAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$bundle->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$bundle->getId().'-SWAF",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    $curl->post('https://api7.publicaster.com/Rest/OptinForms.svc/', $data);
    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }

  /*
  * UPDATE OPTIN SHARE WITH A FRIEND
  */
  public function update_swaf_optin_form($bundle)
  {

    $data = '{
     "FormName":'.json_encode($bundle->getId().' - SWAF - '.$bundle->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$bundle->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
     {
        "FieldValidationType":2,
        "FieldDataType":1,
        "DisplayName":"Email Address",
        "FieldName":"Email",
        "DisplayOrder":1,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"First Name",
        "FieldName":"first_name",
        "DisplayOrder":3,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Last Name",
        "FieldName":"last_name",
        "DisplayOrder":4,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Header Image",
        "FieldName":"header_image",
        "DisplayOrder":10,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"URL",
        "FieldName":"url",
        "DisplayOrder":11,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Button Text",
        "FieldName":"button_text",
        "DisplayOrder":12,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Body1",
        "FieldName":"body1",
        "DisplayOrder":13,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Body2",
        "FieldName":"body2",
        "DisplayOrder":14,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Button Color",
        "FieldName":"button_color",
        "DisplayOrder":15,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Button Font Color",
        "FieldName":"button_font_color",
        "DisplayOrder":16,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Background Color",
        "FieldName":"background_color",
        "DisplayOrder":17,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Font Color",
        "FieldName":"font_color",
        "DisplayOrder":18,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Crowd Background Color",
        "FieldName":"crowd_background_color",
        "DisplayOrder":19,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Style",
        "FieldName":"style",
        "DisplayOrder":20,
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Sent",
        "FieldName":"sent",
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Codes Redeemed",
        "FieldName":"codes_redeemed",
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     },
     {
        "FieldValidationType":0,
        "FieldDataType":1,
        "DisplayName":"Reminder Body1",
        "FieldName":"reminder_body1",
        "PostBackFieldName":"",
        "FacebookConnectFieldMappingName":""
     }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":3,
          "ConfirmationEmailSubject":'.json_encode($bundle->getEmailShareSubject()).',
          "ConfirmationEmailReplyAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$bundle->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$bundle->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$bundle->getId().'-SWAF",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    # get the OptinForm ID
    $pattern = '/OptinFormID=(\d+)&/';
    preg_match($pattern, $bundle->getPostUrlShare(), $matches);

    $curl->put('https://api7.publicaster.com/Rest/OptinForms.svc/'.$matches[1], $data);

    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }


  public function delete_optin_form($optinform_id)
  {

    $curl = $this->curl;
    $curl->del("https://api7.publicaster.com/Rest/OptinForms.svc/$optinform_id/10979");

    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    return $curl->to_json();

  }


  /*
  * GET DISTRIBUTIONS BY CAMPAIGN ID
  */
  public function get_distributions_by_campaign($campaign_id)
  {

    $data = [];
    for ($i=1;$i<=5;$i++) {
      $curl = new Curl;
      $curl->get("https://api7.publicaster.com/Rest/CampaignDistributionsByCampaign.svc/".$campaign_id."/$i");

      if(isset($this->response->Item->OutputMessage)){
        $this->errors = $this->response->Item->OutputMessage;
        return false;
      }

      $distros = $curl->to_json();
      if ($distros) {
        foreach($distros as $distro) {
          array_push($data, $distro);
        }
      }

    }

    return $data;

  }



  /*
  * SEND AN EMAIL
  * Typical Use: CUSTOMER SERVICE AREA - RESEND EMAIL
  */

  public function send_single_email($bundle, $customer) {

    $header_logo_html_array=array(
      '<a href="http://ticketmaster.com" target="_blank"><img src="http://tm-mrs.s3.amazonaws.com/static/ticketmaster_logo.gif" alt="Ticketmaster" border="0"></a>',
      "&nbsp;"
    );

    # make sure bundle has a postURL we can hit to send an email
    if (empty($bundle['postUrl'])) {
      return "missing bundle or postURL";
    }

    if (empty($customer)) {
      return "missing customer";
    }

    $fields = array(
      'UEmail'=>$customer['email'],
      'field6'=>$customer['id'],
      'field7'=>$customer['first_name'],
      'field8'=>$customer['last_name'],
      'field9'=>$customer['event_id'],
      'field10'=>$customer['event_date'],
      'field11'=>$customer['confirmation_number'],
      'field12'=>$customer['location_name'],
      'field13'=>$customer['transaction_type'],
      'field14'=>$customer['qualifier1'],
      'field15'=>$customer['header_image'],
      'field16'=>$customer['url'],
      'field17'=>$bundle['emailButtonText'],
      'field18'=>str_replace("%REDEMPTIONCODES%",$bundle['codes'],$bundle['emailBody1']),
      'field19'=>$bundle['emailBody2'],
      'field20'=>$bundle['emailButtonColor'],
      'field21'=>$bundle['emailButtonFontColor'],
      'field22'=>$bundle['emailBackgroundColor'],
      'field23'=>$bundle['emailFontColor'],
      'field24'=>($bundle['emailStyle']=="dark") ? "#25292a" : "#dad6d5",
      'field25'=>$bundle['emailStyle'],
      'field26'=>$header_logo_html_array[isset($bundle['headerLogoID'])?$bundle['headerLogoID']:0]
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$bundle['postUrl']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);


    if(!curl_errno($ch)) {
      curl_close($ch);
      return true;
    }

    return $response;


  }


  /*
  * SEND AN EMAIL
  */

  public function send_share_email($bundle, $code, $customer) {

    # make sure bundle has a postURL we can hit to send an email
    if (empty($bundle['postUrlShare'])) {
      return "missing bundle or postUrlShare";
    }

    if (empty($code['shareEmail'])) {
      return "missing shareEmail";
    }

    $fields = array(
      'UEmail'=>$code['shareEmail'],
      'field6'=>$customer['id'],
      'field7'=>$customer['first_name'],
      'field8'=>$customer['last_name'],
      'field9'=>$customer['event_id'],
      'field10'=>$customer['event_date'],
      'field11'=>$customer['confirmation_number'],
      'field12'=>$customer['location_name'],
      'field13'=>$customer['transaction_type'],
      'field14'=>$customer['qualifier1'],
      'field15'=>$customer['header_image'],
      'field16'=>PREVIEW_URL."/{$code['bundle_id']}/{$code['code']}?s=".time(),
      'field17'=>$bundle['emailButtonText'],
      'field18'=>$bundle['emailShareBody1'],
      'field19'=>$bundle['emailBody2'],
      'field20'=>$bundle['emailButtonColor'],
      'field21'=>$bundle['emailButtonFontColor'],
      'field22'=>$bundle['emailBackgroundColor'],
      'field23'=>$bundle['emailFontColor'],
      'field24'=>($bundle['emailStyle']=="dark") ? "#25292a" : "#dad6d5",
      'field25'=>$bundle['emailStyle']
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$bundle['postUrlShare']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);


    if(!curl_errno($ch)) {
      curl_close($ch);
      return true;
    }

    return $response;


  }

  /*
  * CREATE THIRD PARTY OPTIN FORM
  */
  public function create_third_party_optin_form($campaign)
  {

    $data = '{
     "FormName":'.json_encode($campaign->getId().' - '.$campaign->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$campaign->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
         {
            "FieldValidationType":2,
            "FieldDataType":1,
            "DisplayName":"Email Address",
            "FieldName":"Email",
            "DisplayOrder":1,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"id",
            "FieldName":"id",
            "DisplayOrder":2,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"First Name",
            "FieldName":"first_name",
            "DisplayOrder":3,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Last Name",
            "FieldName":"last_name",
            "DisplayOrder":4,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event ID",
            "FieldName":"event_id",
            "DisplayOrder":5,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"event_date",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"confirmation_number",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Location Name",
            "FieldName":"location_name",
            "DisplayOrder":7,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Transaction Type",
            "FieldName":"transaction_type",
            "DisplayOrder":8,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Qualifier1",
            "FieldName":"qualifier1",
            "DisplayOrder":9,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Image",
            "FieldName":"header_image",
            "DisplayOrder":10,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"URL",
            "FieldName":"url",
            "DisplayOrder":11,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Text",
            "FieldName":"button_text",
            "DisplayOrder":12,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body1",
            "FieldName":"body1",
            "DisplayOrder":13,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body2",
            "FieldName":"body2",
            "DisplayOrder":14,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Color",
            "FieldName":"button_color",
            "DisplayOrder":15,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Font Color",
            "FieldName":"button_font_color",
            "DisplayOrder":16,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Background Color",
            "FieldName":"background_color",
            "DisplayOrder":17,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Font Color",
            "FieldName":"font_color",
            "DisplayOrder":18,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Crowd Background Color",
            "FieldName":"crowd_background_color",
            "DisplayOrder":19,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Style",
            "FieldName":"style",
            "DisplayOrder":20,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Logo",
            "FieldName":"header_logo",
            "DisplayOrder":21,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":1,
          "ConfirmationEmailSubject":'.json_encode($campaign->getEmailSubject()).',
          "ConfirmationEmailReplyAddressID":'.$campaign->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$campaign->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$campaign->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$campaign->getId().'-Redemption",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    $curl->post('https://api7.publicaster.com/Rest/OptinForms.svc/', $data);

    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }

  /*
  * UPDATE OPTIN FORM
  */
  public function update_third_party_optin_form($campaign)
  {

    $data = '{
     "FormName":'.json_encode($campaign->getId().' - '.$campaign->getName()).',
     "FormType":2,
     "MailingListMappingIDs":[
        '.$campaign->getListId().'
     ],
     "CreatedBy":10979,
     "ModifiedBy":10979,
     "EnableCookielessConversions":true,
     "OnlyAddNewSubscribersDoNotUpdateExistingSubscribers":false,
     "EnableFacebookConnect":false,
     "Fields":[
         {
            "FieldValidationType":2,
            "FieldDataType":1,
            "DisplayName":"Email Address",
            "FieldName":"Email",
            "DisplayOrder":1,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"id",
            "FieldName":"id",
            "DisplayOrder":2,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"First Name",
            "FieldName":"first_name",
            "DisplayOrder":3,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Last Name",
            "FieldName":"last_name",
            "DisplayOrder":4,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event ID",
            "FieldName":"event_id",
            "DisplayOrder":5,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"event_date",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Event Date",
            "FieldName":"confirmation_number",
            "DisplayOrder":6,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Location Name",
            "FieldName":"location_name",
            "DisplayOrder":7,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Transaction Type",
            "FieldName":"transaction_type",
            "DisplayOrder":8,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Qualifier1",
            "FieldName":"qualifier1",
            "DisplayOrder":9,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Image",
            "FieldName":"header_image",
            "DisplayOrder":10,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"URL",
            "FieldName":"url",
            "DisplayOrder":11,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Text",
            "FieldName":"button_text",
            "DisplayOrder":12,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body1",
            "FieldName":"body1",
            "DisplayOrder":13,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Body2",
            "FieldName":"body2",
            "DisplayOrder":14,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Color",
            "FieldName":"button_color",
            "DisplayOrder":15,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Button Font Color",
            "FieldName":"button_font_color",
            "DisplayOrder":16,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Background Color",
            "FieldName":"background_color",
            "DisplayOrder":17,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Font Color",
            "FieldName":"font_color",
            "DisplayOrder":18,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Crowd Background Color",
            "FieldName":"crowd_background_color",
            "DisplayOrder":19,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Style",
            "FieldName":"style",
            "DisplayOrder":20,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         },
         {
            "FieldValidationType":0,
            "FieldDataType":1,
            "DisplayName":"Header Logo",
            "FieldName":"header_logo",
            "DisplayOrder":21,
            "PostBackFieldName":"",
            "FacebookConnectFieldMappingName":""
         }
       ],
       "SignupSettings":{
          "RedirectURL":"",
          "RedirectToSubscriberPreferencesPageAfterSignup":false,
          "RedirectLandingPageID":0,
          "TransactionalEmail":true,
          "AlreadyOptedOutRedirectURL":"",
          "PostBackURL":"",
          "UsePostBackURL":false,
          "EmailAddressPostBackFieldName":"",
          "ConfirmationEmailContentID":1,
          "ConfirmationEmailSubject":'.json_encode($campaign->getEmailSubject()).',
          "ConfirmationEmailReplyAddressID":'.$campaign->getEmailSendFrom().',
          "ConfirmationEmailFromAddressID":'.$campaign->getEmailSendFrom().',
          "ConfirmationEmailPreheader":"",
          "ConfirmationEmailCampaignID":'.$campaign->getCampaignId().',
          "ConfirmationEmailEnableGoogleAnalytics":true,
          "ConfirmationEmailGoogleAnalyticsTitle":"'.$campaign->getId().'-Redemption",
          "ConfirmationEmailEnableEyeView":true,
          "ConfirmationEmailAdditionalLinkTrackingParameters":"",
          "PhysicalMailingAddressID":1
       },
       "SignupNotificationSettings":{
          "SignupNotificationEmailRecipients":"",
          "SignupNotificationEmailSubject":"",
          "ReceiveSignupNotificationEmails":false,
          "PickRandomNotificationRecipient":false
       }
    }';


    $curl = $this->curl;

    # get the OptinForm ID
    $pattern = '/OptinFormID=(\d+)&/';
    preg_match($pattern, $bundle->getPostUrl(), $matches);

    $curl->put('https://api7.publicaster.com/Rest/OptinForms.svc/'.$matches[1], $data);

    $this->response = $curl->to_json();
    if(isset($this->response->Item->OutputMessage)){
      $this->errors = $this->response->Item->OutputMessage;
      return false;
    }

    preg_match_all('/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*(OptinFormId)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i', $this->response->SampleHtmlFormCode, $result, PREG_PATTERN_ORDER);
    $result = $result[0];

    return $result[0];

  }

  /*
  * SEND AN EMAIL
  * Typical Use: CUSTOMER SERVICE AREA - RESEND EMAIL
  */

  public function send_single_third_party_email($campaign, $customer) {

    # make sure bundle has a postURL we can hit to send an email
    if (empty($campaign['postUrl'])) {
      return "missing third part bundle or postURL";
    }

    if (empty($customer)) {
      return "missing customer";
    }

    $header_logo_html_array=array(
      '<a href="http://ticketmaster.com" target="_blank"><img src="http://tm-mrs.s3.amazonaws.com/static/ticketmaster_logo.gif" alt="Ticketmaster" border="0"></a>',
      "&nbsp;"
    );

    $fields = array(
      'UEmail'=>$customer['email'],
      'field6'=>$customer['id'],
      'field7'=>$customer['first_name'],
      'field8'=>$customer['last_name'],
      'field9'=>$customer['event_id'],
      'field10'=>$customer['event_date'],
      'field11'=>$customer['confirmation_number'],
      'field12'=>$customer['location_name'],
      'field13'=>$customer['transaction_type'],
      'field14'=>$customer['qualifier1'],
      'field15'=>$customer['header_image'],
      'field16'=>$customer['url'],
      'field17'=>$campaign['emailButtonText'],
      'field18'=>str_replace("%REDEMPTIONCODES%",$campaign['codes'],$campaign['emailBody1']),
      'field19'=>$campaign['emailBody2'],
      'field20'=>$campaign['emailButtonColor'],
      'field21'=>$campaign['emailButtonFontColor'],
      'field22'=>$campaign['emailBackgroundColor'],
      'field23'=>$campaign['emailFontColor'],
      'field24'=>($campaign['emailStyle']=="dark") ? "#25292a" : "#dad6d5",
      'field25'=>$campaign['emailStyle'],
      'field26'=>$header_logo_html_array[isset($campaign['headerLogoID'])?$campaign['headerLogoID']:0]
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$campaign['postUrl']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);


    if(!curl_errno($ch)) {
      curl_close($ch);
      return true;
    }

    return $response;


  }



}
