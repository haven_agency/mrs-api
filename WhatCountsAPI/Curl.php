<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Utility;

class Curl
{
  public $curl;
  private $header;
  public $response;
  public $errors;
  
  public function __construct($account=false) {
      
    $env = getenv('APP_ENV') ? : 'development';
  
    $credentials = WHATCOUNTS_CREDENTIALS_STAGING; // staging
    if ($env == 'production') {
      $credentials = WHATCOUNTS_CREDENTIALS_PRODUCTION; // production
    }
    
    if ($account=="thirdparty") {
      $credentials = WHATCOUNTS_CREDENTIALS_THIRD_PARTY_STAGING; // 3rd party staging
      if ($env == 'production') {
        $credentials = WHATCOUNTS_CREDENTIALS_THIRD_PARTY_PRODUCTION; //  3rd party production
      }
    }
    
    $header = array();
    //$header[] = 'Content-length: 0';
    $header[] = "Content-type: application/json";
    $header[] = "Authorization: $credentials";
    
    $this->curl = curl_init();
    curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);

  }
  
  public function get($url)
  {
    curl_setopt($this->curl, CURLOPT_URL, $url."?format=json"); 
    $this->response = $this->exec();
  }
  
  public function post($url, $data=null)
  {
    curl_setopt($this->curl, CURLOPT_URL, $url."?format=json"); 
    
    curl_setopt($this->curl, CURLOPT_POST, true);
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    $this->response = $this->exec();

  }
  
  
  public function put($url, $data=null)
  {
    curl_setopt($this->curl, CURLOPT_URL, $url."?format=json"); 
    
    curl_setopt($this->curl, CURLOPT_POST, true);
    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    $this->response = $this->exec();

  }
  
  public function del($url)
  {
    curl_setopt($this->curl, CURLOPT_URL, $url."?format=json"); 
    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    $this->response = $this->exec();
    return $this->response;
  }
  
  
  public function exec()
  {
    
    $curl_response = curl_exec($this->curl);

    if (curl_error($this->curl)) {
      $this->errors = curl_error($this->curl);
    }

    return $curl_response;
    
  }
  
  public function to_json()
  {
    
    return json_decode($this->response);
    
  }
  
  public function get_info() {
    return curl_getinfo($this->curl);
  }
  
}