<?php

namespace Mrs\Api;

use \Application\Utility\WhatcountsAPI;

class BundleOverviewAPI
{
    protected $q;
    protected $adapter;
    public $wc;

    protected static $cache_keys = array(
        'getRedemptionDistributions' => 'bundle:%d:redemptionDistribution',
        'get_past_unique' => 'bundle:%d:pastUnique',
        'get_present_unique' => 'bundle:%d:presentUnique:%d-%s-%s',
        'get_stats' => 'bundle:%d:stats',
        'get_region' => 'bundle:%d:regions:$s',
        'get_cached_at' => 'bundle:%d:cached_at',
    );

    public function __construct($dbAdapter)
    {
        $this->adapter = $dbAdapter;
        $this->redis = new \Redis();
        $this->redis->pconnect('127.0.0.1', 6379);
    }

    public function invalidateCacheBundleOverview($bundle_id)
    {
        $keys = $this->redis->hKeys('BundleOverview');
        $invalidate_keys = array_filter($keys, function($v) use ($bundle_id) {
           return stristr($v, $bundle_id);
        });

        array_unshift($invalidate_keys, 'BundleOverview');
        call_user_func_array(array($this->redis, 'hDel'), $invalidate_keys);
    }

    
    public function recacheActiveBundles()
    {
        $bundles = $this->getActiveBundles();
        if (!$bundles) return false;

        foreach ($bundles as $bundle) {
            logger("Recaching: ".$bundle['id']);
            # pass recache flag as true
            $this->get_stats($bundle['id'], '', true);
        }

    }
    public function getActiveBundles()
    {
        $bundles = $this->adapter->query("SELECT b.* FROM bundles b WHERE b.`campaignEndTime` > NOW() OR b.`physicalEndTime` > NOW() OR b.`digitalEndTime` > NOW() ORDER BY b.id DESC", [])->toArray();
        return $bundles;
    }

    public function fullReCache()
    {
        $fullCacheKeys = $this->redis->hKeys('BundleOverview');

        foreach (self::$cache_keys as $func => $format)
        {
            $search = explode(':', $format);
            $search = $search[0];
            $subSetCacheKeys = array_filter($fullCacheKeys, function($v) use ($search) {
                return stristr($v, $search);
            });

            foreach ($subSetCacheKeys as $redisKey)
            {
                $redisArgs = sscanf($redisKey, $format);
                call_user_func_array(array($this, $func), $redisArgs);
            }
        }
    }


    protected function redisKey($func, $args = array())
    {
        return vsprintf(self::$cache_keys[$func], $args);
    }

    protected function redisFetch($redisKey)
    {
        $rawRedisValue = $this->redis->hGet('BundleOverview', $redisKey);
        return ($rawRedisValue) ? json_decode($rawRedisValue, true) : $rawRedisValue;
    }

    public function getRedemptionDistributions($bundle_id)
    {
        $redisKey = $this->redisKey(__FUNCTION__, [$bundle_id]);

        if (!$cached = $this->redisFetch($redisKey))
        {
            $distro_id= array();
            $result = $this->adapter->query("SELECT id, distributionId, campaignId FROM `bundles` b WHERE b.id=?",[$bundle_id])->toArray()[0];
            if ($result['campaignId']) {
                $wc  = new WhatcountsAPI();
                $distros = $wc->get_distributions_by_campaign($result['campaignId']);
                foreach ($distros as $distro) {
                    if ($distro->Item->SelectedEmail == 1) {
                        $distro_id[] = $distro->Item->CampaignDistributionID;
                    }
                }

            }

            $this->redis->hSet('BundleOverview', $redisKey, json_encode($distro_id));
            return $distro_id;
        }
        else
        {
            return $cached;
        }
    }

    public function get_past_unique($bundle_id, $email_template_id=1) {
        
        $table = "";
        if ($email_template_id==2) {
            $table = "reminder_";
        }

        $redisKey = $this->redisKey(__FUNCTION__, [$bundle_id, $email_template_id]);

        if (!$cached = $this->redisFetch($redisKey))
        {

            $excluded_o = $this->adapter->query("SELECT DISTINCT distribution_detail_id used_details FROM `wc_{$table}opens` o JOIN `bundles` b ON b.campaignID = o.campaign_id WHERE b.id = ?",[$bundle_id])->toArray();
            $excluded_c = $this->adapter->query("SELECT DISTINCT distribution_detail_id used_details FROM `wc_{$table}clicks` c JOIN `bundles` b ON b.campaignID = c.campaign_id WHERE b.id = ? ",[$bundle_id])->toArray();

            #create single dimensional array of unique ddids
            $exo = [];
            foreach ($excluded_o as $key=>$value) {
                foreach ($value as $k2 => $v2) {
                    //array_push($exo, $v2);
                    $exo[] = (int)$v2;
                }
            }
            $exc = [];
            foreach ($excluded_c as $key=>$value) {
                foreach ($value as $k2 => $v2) {
                    //array_push($exc, $v2);
                    $exc[] = (int)$v2;
                }
            }

            $past_unique = array('excluded_opens'=>$exo,
                'excluded_clicks'=>$exc);
            $this->redis->hSet('BundleOverview', $redisKey, json_encode($past_unique));
            return $past_unique;
        }
        else
        {
            return $cached;
        }
    }

    public function get_array_from_api($action,$distro_id,$start_date,$end_date,$p,$page_size) {

        $wc = new WhatcountsAPI();
        switch ($action){
            case 'clicks':
                $json = $wc->get_clicks($distro_id,$start_date,$end_date,$p,$page_size);
                $array = get_object_vars($json);
                $array = $array['Clicks'];
                break;

            case 'opens':
                $json = $wc->get_opens($distro_id,$start_date,$end_date,$p,$page_size);
                $array = get_object_vars($json);
                $array = $array['Opens'];
                break;
        }

        $x = [];
        $rows=0;
        foreach ($array as $key=>$value) {
            foreach ($value as $k2 => $v2) {
                if($k2=="DistributionDetailID") {
                    $x[] = (int)$v2;
                    //array_push($x, $v2);
                }

            }
            $rows++;
        }

        $finished =($rows<$page_size?true:false);

        return array('ids'=>$x,
            'finished'=>$finished);

    }

    public function get_present_unique($bundle_id, $distro_id, $excluded_opens, $excluded_clicks) {


        $date = new \DateTime();
        $date->add(\DateInterval::createFromDateString('today'));

        $today = $date->format('mdY');// format like this "06012015";
        $date->modify('+1 day');
        $tomorrow = $date->format('mdY');
        $start_date = $today; //"06011990";//
        $end_date = $tomorrow;

        $page_size=500;
        $p=1;
        $total_unique_opens = 0;
        $to=array();
        $distros = $this->getRedemptionDistributions($bundle_id);

        // generate unique opens
        foreach ($distros as $distro_id) {
            $o = $this->get_array_from_api('opens',$distro_id,$start_date,$end_date,$p,$page_size);
            $finished = $o['finished'];
            $o=$o['ids'];

            while ($finished==false) {

                $p++;
                $merge = $this->get_array_from_api('opens',$distro_id,$start_date,$end_date,$p,$page_size);
                $finished = $merge['finished'];

                //$o = $o + $merge['ids'];
                $o = array_merge($o,$merge['ids']);

            }


            $to = array_merge($excluded_opens, $o, $to);
            $to = array_unique($to, SORT_NUMERIC);

        }
        $to = count($to);
        $total_unique_opens = $to;
        $tc = array();
        $total_unique_clicks = 0;
        foreach ($distros as $distro_id) {
            //generate unique clicks
            $merge = [];
            $p=1;

            $c = $this->get_array_from_api('clicks',$distro_id,$start_date,$end_date,$p,$page_size);
            $finished = $c['finished'];
            $c=$c['ids'];

            while ($finished==false) {

                $p++;
                $merge = $this->get_array_from_api('clicks',$distro_id,$start_date,$end_date,$p,$page_size);
                $finished = $merge['finished'];

                //$c = $c + $merge['ids'];
                $c = array_merge($c,$merge['ids']);


            }

            //combine ddid's from db and api
            // $tc = //array_merge($tc, $c);
            $tc = array_merge($excluded_clicks, $c, $tc);

            #error_log(count($tc));
            $tc = array_unique($tc,SORT_NUMERIC);

        }
        $tc = count($tc);
        $total_unique_clicks = $tc;

        $present_unique =  array('clicks'=>$total_unique_clicks, 'opens'=>$total_unique_opens);

        return $present_unique;


    }


    public function get_unique($bundle_id, $distro_id, $email_template_id=1) {

        //get unique clicks and opens from the db and API
        $past = $this->get_past_unique($bundle_id, $email_template_id);
        //add unique clicks and opens from the api for today's data
        $unique = $this->get_present_unique($bundle_id, $distro_id,$past['excluded_opens'],$past['excluded_clicks']);

        return array('clicks'=>($unique['clicks']),
            'opens'=>($unique['opens']));

    }


    public function get_region($id)
    {
        $region = $this->adapter->query("SELECT r.* FROM bundles b JOIN regions r ON r.id = b.region_id WHERE b.id = ?",[$id])->current();
        return $region;

    }

    public function get_cached_at($id)
    {
         $redisKey = $this->redisKey(__FUNCTION__, [$id]);
         $cached = $this->redisFetch($redisKey);
         if ($cached)
         {
           return $cached;
         }

         return false;
    }

    public function get_stats($id, $date_range='', $recache=false)
    {

            if (empty($id)) return false;

            $redisKey = $this->redisKey(__FUNCTION__, [$id]);
            $cached = $this->redisFetch($redisKey);
            //$cached = false;
            if ($recache || !$cached || (is_array($date_range) && $date_range[0] && $date_range[1]))
            {

            $params = [
                ':bundle_id'=>$id
            ];

            $bundle = $this->adapter->query("SELECT * FROM `bundles` b WHERE b.id=? ",[$id])->current();


            # Get total codes generated
            $total_codes = $this->adapter->query("SELECT COUNT(*) total FROM `bundle_codes` c WHERE c.bundle_id=? {$this->build_date_query('c' ,$date_range)} ",[$id])->current()['total'];
            $total_sent = $this->adapter->query("SELECT SUM(c.total_codes) total FROM `customers` c WHERE c.bundle_id=? {$this->build_date_query('c', $date_range)} ",[$id])->current()['total'];

            # get redemptions totals
            $total_redemptions = $this->adapter->query("SELECT SUM(r.quantity) as total FROM `redemptions` r WHERE r.bundle_id=? {$this->build_date_query('r', $date_range)} AND r.unredeemed=0",[$id])->current()['total'];
            $total_digital_redemptions = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r WHERE r.bundle_id=? AND r.bundle_download_id IS NOT NULL {$this->build_date_query('r', $date_range)} AND r.unredeemed=0",[$id])->current()['total'];
            $total_physical_redemptions = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r WHERE r.bundle_id=? AND r.bundle_product_id IS NOT NULL {$this->build_date_query('r', $date_range)} AND r.unredeemed=0",[$id])->current()['total'];
            $total_upsell_redemptions = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r WHERE r.bundle_id=? AND r.bundle_upsell_id IS NOT NULL {$this->build_date_query('r', $date_range)} AND r.unredeemed=0",[$id])->current()['total'];

            # get customeres which havent redeemed any
            $total_redeemed_none_customers = $this->adapter->query("SELECT COUNT(*) total FROM (
        SELECT COUNT(c.id) total, sum(codes.used) codes_used
        FROM `customers` c INNER JOIN `bundle_codes` codes ON codes.customer_id=c.id
        WHERE c.bundle_id=? {$this->build_date_query('c', $date_range)}
        GROUP BY c.id
        HAVING codes_used = 0) as customers",[$id])->current()['total'];
            //c.last_sent_at IS NOT NULL AND

            # get customers with some codes left
            $total_redeemed_some_customers = $this->adapter->query("SELECT COUNT(*) total FROM (
        SELECT COUNT(c.id) total, c.total_codes, sum(codes.used) codes_used
        FROM `customers` c INNER JOIN `bundle_codes` codes ON codes.customer_id=c.id
        WHERE c.bundle_id=? {$this->build_date_query('c', $date_range)}
        GROUP BY c.id
        HAVING codes_used > 0 AND codes_used < c.total_codes) as customers",[$id])->current()['total'];
            //c.last_sent_at IS NOT NULL AND

            $total_redeemed_all_customers = $this->adapter->query("SELECT COUNT(*) total FROM (
        SELECT COUNT(c.id) total, c.total_codes, sum(codes.used) codes_used
        FROM `customers` c INNER JOIN `bundle_codes` codes ON codes.customer_id=c.id
        WHERE c.bundle_id=? {$this->build_date_query('c', $date_range)}
        GROUP BY c.id
        HAVING codes_used = c.total_codes) as customers",[$id])->current()['total'];

            # Get digital variants with upc/isrc data for each download
            $download_options = $this->adapter->query("SELECT d.id, d.name
        FROM `bundle_downloads` d
        WHERE d.bundle_id=? {$this->build_date_query('d', $date_range)}
        ORDER BY d.id DESC",[$id])->toArray();

            $total_download_percent = 0;

            foreach($download_options as &$download) {
               
                # Get redemption count for each download.. restrict by upc/isrc country if present, otherwise ignore
                if (isset($download['upc_country']) || isset($download['isrc_country'])) {
                    $total = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r
            WHERE r.bundle_download_id=? AND r.unredeemed=0 AND r.country=? {$this->build_date_query('r', $date_range)}", [$download['id'], $download['upc_country']?: $download['isrc_country']])->current()['total'];
                } else {
                    $total = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r
            WHERE r.bundle_download_id=? AND r.unredeemed=0 {$this->build_date_query('r', $date_range)}", [$download['id']])->current()['total'];
                }

                $download['total'] = $total;
                $percent = $total ? number_format(100*($total/$total_digital_redemptions)) : 0;
                $download['percent'] = $percent;
                $total_download_percent += $percent;
            }
           

            $warn_insufficient_upcs = count($download_options) > 0 && $total_download_percent < 100;


            # Get physical variants and their redemption counts
            $product_options = $this->adapter->query("SELECT p.name, p.sku, SUM(r.quantity) total, p.country,  ROUND( ( CHAR_LENGTH(sku) - CHAR_LENGTH( REPLACE ( sku, ',', '') )) / CHAR_LENGTH(',')+1) as sku_count
        FROM `bundle_products` p
        INNER JOIN `redemptions` r ON p.id=r.bundle_product_id
        WHERE p.bundle_id=? {$this->build_date_query('r', $date_range)} AND r.unredeemed=0
        GROUP BY p.name ORDER BY p.id DESC",[$id])->toArray();

            if ($product_options && $product_options[0]['total']) {
                foreach($product_options as &$product) {
                    $product['percent'] = number_format(100*($product['total']/$total_physical_redemptions));
                }
            } else {
                foreach($product_options as &$product) {
                    $product['total'] = 0;
                    $product['percent'] = 0;
                }
            }

            
            # Get upsells variants and their redemption counts
            $upsell_options = $this->adapter->query("SELECT u.name, u.sku, SUM(r.quantity) total, u.country,  ROUND( ( CHAR_LENGTH(sku) - CHAR_LENGTH( REPLACE ( sku, ',', '') )) / CHAR_LENGTH(',')+1) as sku_count
        FROM `bundle_upsells` u
        INNER JOIN `redemptions` r ON u.id=r.bundle_upsell_id
        WHERE u.bundle_id=? {$this->build_date_query('r', $date_range)} AND r.unredeemed=0
        GROUP BY u.name ORDER BY u.id DESC",[$id])->toArray();

            if ($upsell_options && $upsell_options[0]['total']) {
                foreach($upsell_options as &$upsell) {
                    $upsell['percent'] = number_format(100*($upsell['total']/$total_upsell_redemptions));
                }
            } else {
                foreach($upsell_options as &$upsell) {
                    $upsell['total'] = 0;
                    $upsell['percent'] = 0;
                }
            }

            # Count total optins
            $optins = $this->adapter->query("SELECT count(*) total FROM `redemptions` r WHERE r.bundle_id=? {$this->build_date_query('r', $date_range)} AND (r.optinArtist=1 OR r.optinTicketmaster=1) AND r.unredeemed=0",[$id])->current()['total'];

            # Count total contest entries
            $contest_entries = $this->adapter->query("SELECT count(*) total FROM `redemptions` r WHERE r.bundle_id=? {$this->build_date_query('r', $date_range)} AND r.contestEntry=1 AND r.unredeemed=0",[$id])->current()['total'];

            # Count total primary redemtpions
            $primary = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r LEFT JOIN `bundle_codes` c ON r.code_id=c.id WHERE r.bundle_id = ? AND c.primary=1 {$this->build_date_query('r', $date_range)} AND r.unredeemed=0",[$id])->current()['total'];

            # Count total secondary redemptions
            $secondary = $this->adapter->query("SELECT SUM(r.quantity) total FROM `redemptions` r LEFT JOIN `bundle_codes` c ON r.code_id=c.id WHERE r.bundle_id = ? AND (c.primary IS NULL or c.primary=0) {$this->build_date_query('r', $date_range)} AND r.unredeemed=0",[$id])->current()['total'];


            # Get total redemptions(redeemed codes) and how many codes sent to each country
            $countries = $this->adapter->query("SELECT
        (SELECT SUM(cust.total_codes) total
        FROM `customers` cust
        LEFT JOIN transactions t ON  t.id = cust.transaction_id
        WHERE t.country = c.code AND cust.bundle_id=? {$this->build_date_query('t', $date_range)} ) total_sent,
        (SELECT COUNT(*) FROM `customers` cust
        LEFT JOIN transactions t ON  t.id = cust.transaction_id
        WHERE t.country = c.code AND cust.bundle_id = ?) total_email_sent,
        c.name,
        SUM(r.quantity) total_redeemed,
        SUM(CASE WHEN r.bundle_download_id > 0 THEN r.quantity ELSE 0 END) total_digital,
        SUM(CASE WHEN r.bundle_product_id > 0 THEN r.quantity ELSE 0 END) total_physical,
        SUM(CASE WHEN r.bundle_upsell_id > 0 THEN r.quantity ELSE 0 END) total_upsells
        FROM redemptions r
        LEFT JOIN countries c ON c.code=r.country
        WHERE r.bundle_id = ? AND r.unredeemed = 0 {$this->build_date_query('r', $date_range)}
        GROUP BY r.country", [$id,$id,$id])->toArray();
        


            foreach ($countries as &$country) {
                $country['percent_redeemed'] = $total_redemptions ? number_format(100*($country['total_redeemed'] / $total_redemptions)) : 0;
                $country['percent_redeemed_returns'] = $country['total_sent']  ? number_format(100*($country['total_redeemed'] / $country['total_sent'])) : 0;
                $country['percent_digital'] = $country['total_redeemed'] ? number_format(100*($country['total_digital'] / $country['total_redeemed'])) : 0;
                $country['percent_physical'] = $country['total_redeemed'] ? number_format(100*($country['total_physical'] / $country['total_redeemed'])) : 0;
                $country['percent_upsells'] = $country['total_upsells'] ? number_format(100*($country['total_upsells'] / $total_upsell_redemptions)) : 0;
            }

            # Get Customers and events/venues per customer
            $total_customers = $this->adapter->query("SELECT count(c.id) total FROM `customers` c WHERE c.bundle_id = ? {$this->build_date_query('c', $date_range)}",[$id])->current()['total'];
            $customer_events = $this->adapter->query("SELECT c.event_id id, c.location_name name, c.event_date date, count(c.id) total_customers, sum(c.total_codes) total_codes FROM `customers` c WHERE c.bundle_id = ? {$this->build_date_query('c', $date_range)} GROUP BY c.event_id ORDER BY c.event_date DESC",[$id])->toArray();
            foreach($customer_events as &$event){
                $total_redeemed = $this->adapter->query( "SELECT SUM(r.quantity) total, COUNT(r.id) total_rows,
                    SUM(CASE WHEN r.bundle_download_id > 0 THEN r.quantity ELSE 0 END) digital,
                    SUM(CASE WHEN r.bundle_product_id > 0 THEN r.quantity ELSE 0 END) physical
                    FROM `redemptions` r
                    INNER JOIN `bundle_codes` codes ON codes.id=r.code_id
                    LEFT JOIN `customers` ON customers.id=codes.customer_id
                    WHERE r.bundle_id=? {$this->build_date_query('r', $date_range)} AND r.unredeemed=0 AND customers.event_id=? LIMIT 1",
                    [$id, $event['id']])->toArray()[0];
                $event['total_redeemed'] = number_format($total_redeemed['total']);
                $event['total_physical'] = number_format($total_redeemed['physical']);
                $event['total_digital'] = number_format($total_redeemed['digital']);
                $event['percent_redeemed'] = number_format(100*($total_redeemed['total']/$event['total_codes']));
                $event['percent_digital'] = $total_redeemed['digital'] ? number_format(100*($total_redeemed['digital']/$total_redeemed['total_rows'])): 0;
                $event['percent_physical'] = $total_redeemed['physical'] ? number_format(100*($total_redeemed['physical']/$total_redeemed['total_rows'])) : 0;
            }


            # Get Transactions and events/venues per transaction
            $total_transactions = $this->adapter->query("SELECT count(t.id) total FROM `transactions` t WHERE t.bundle_id = ? {$this->build_date_query('t', $date_range)}",[$id])->current()['total'];
            $transaction_events = [];
            
            
            # Get successful Mainstreet fulfilment API submissions
            $mainstreet_submissions = $this->adapter->query("SELECT 
              COUNT(CASE WHEN up.exported_at IS NOT NULL THEN 1 END) AS successes, 
              COUNT(CASE WHEN up.exported_at IS NULL THEN 1 END) AS fails
              FROM upsell_payments up 
              INNER JOIN redemptions r ON r.id=up.redemption_id 
              WHERE r.bundle_id = ? AND r.unredeemed=0 ".$this->build_date_query('r', $date_range), [$id])->current();
            $mainstreet_success = $mainstreet_submissions['successes'];
            $mainstreet_failed = $mainstreet_submissions['fails'];
                
            # Get successful MusicToday redemptions
            $mt = $this->adapter->query("SELECT 
              COUNT(CASE WHEN mtl.status=1 THEN 1 END) AS successes, 
              COUNT(CASE WHEN mtl.status=0 AND mtl.master_attempt_id IS NULL AND mtl.notified=1 THEN 1 END) AS fails 
              FROM musictoday_api_log mtl 
              LEFT JOIN redemptions r ON r.id=mtl.redemption_id 
              WHERE r.bundle_id = ? AND r.unredeemed=0 ".$this->build_date_query('r', $date_range), [$id])->current();
            $mt_invalid_notified = $this->adapter->query("SELECT SUM(mtl1.notified) total FROM musictoday_api_log mtl1 LEFT JOIN redemptions r ON r.id=mtl1.redemption_id WHERE mtl1.status=-1 AND r.bundle_id = ? AND r.unredeemed=0 ".$this->build_date_query('r', $date_range), [$id])->current()['total'];
            $mt_fail_retries = $this->adapter->query("SELECT COUNT(mtl2.id) total FROM musictoday_api_log mtl1 LEFT JOIN musictoday_api_log mtl2 ON mtl2.master_attempt_id=mtl1.id LEFT JOIN redemptions r ON r.id=mtl1.redemption_id WHERE mtl1.status=0 AND r.bundle_id = ? AND r.unredeemed=0 AND mtl1.master_attempt_id IS NULL ".$this->build_date_query('r', $date_range), [$id])->current()['total'];
            $mt_redemptions = $mt['successes'];
            $mt_failed_redemptions = $mt['fails'];
            // Note that this invalid count is adjusted to ignore any invalid calls that we forced a retry on (ex: bad SKU on the bundle)
            $mt_invalid_redemptions = $this->adapter->query("SELECT COUNT(mtl1.id) - COUNT(CASE WHEN mtl2.status=1 THEN 1 END) total FROM musictoday_api_log mtl1 LEFT JOIN musictoday_api_log mtl2 ON mtl2.master_attempt_id=mtl1.id LEFT JOIN redemptions r ON r.id=mtl1.redemption_id WHERE mtl1.status=-1 AND r.bundle_id = ? AND r.unredeemed=0 AND mtl1.master_attempt_id IS NULL ".$this->build_date_query('r', $date_range), [$id])->current()['total'];

            // Digital reporting (Soundscan and Official Charts)
            $digital_reporting = [
                'soundscan' => number_format(0),
                'official_charts' => number_format(0)
            ];

            # Get Soundscan exports
                $date_rng_start = is_array($date_range) && array_key_exists(0, $date_range) && strlen($date_range[0])>0 ? $date_range[0].' 00:00:00' : '2012-01-01 00:00:00';
                $date_rng_end = is_array($date_range) && array_key_exists(1, $date_range) && strlen($date_range[1])>0 ? $date_range[1].' 23:59:59' : date('Y-m-d 23:59:59');

            $digital_reporting['soundscan'] = $this->adapter->query("SELECT (SELECT count(r.id) FROM `redemptions` r WHERE r.bundle_id=? AND r.soundscanExportedAt BETWEEN ? AND ?)+(SELECT IFNULL(SUM(t.ticketsPurchased),0) FROM `transactions` t LEFT JOIN `customers` c ON t.customer_id=c.id WHERE c.bundle_id=? AND c.soundscan_exported_at BETWEEN ? AND ?) AS total",[$id, $date_rng_start, $date_rng_end, $id, $date_rng_start, $date_rng_end])->current()['total'];

            $digital_reporting['official_charts'] = $this->adapter->query("SELECT count(r.id) total FROM `redemptions` r WHERE r.bundle_id=? AND r.officialChartsExportedAt BETWEEN ? AND ?",[$id, $date_rng_start, $date_rng_end])->current()['total'];

            # Get redemption chart data
            $region = $this->get_region($id);

            $tdt = new \DateTime('today',new \DateTimeZone($region['timezone']));
            $tdt->modify('-7 days');
            $start_date = $tdt->format('Y-m-d');
            $tdt->modify('+7 days');
            $end_date = $tdt->format('Y-m-d');

            //$start_date = date('Y-m-d', strtotime('-7 days'));
            //$end_date = date('Y-m-d');
            $rd = $this->get_redemption_graph_data($bundle['id'], $start_date, $end_date);

            $redemptions_over_time_categories = [];
            $redemptions_over_time_data = [];
            foreach ($rd as $date => $r_data)
            {
                $redemptions_over_time_categories[] = date('m/d', strtotime($date));
                $redemptions_over_time_data[] = (int)$r_data;
            }

            $stats = [
                //'bundle'=>$bundle,
                'codes'=>[
                    'total'=>number_format($total_codes),
                    'sent'=>[
                        'total'=>number_format($total_sent),
                        'percent'=> ($total_codes&&$total_sent) ? number_format(100*($total_sent/$total_codes)) : 0
                    ],
                    'unsent'=>[
                        'total'=>number_format($total_codes-$total_sent),
                        'percent'=> ($total_codes&&$total_sent) ? number_format(100*(($total_codes-$total_sent)/$total_codes)) : 0
                    ],
                ],
                'redemptions'=>[
                    'total'=>number_format($total_redemptions),
                    'percent'=> ($total_redemptions&&$total_codes) ? number_format(100*($total_redemptions/$total_codes)) : 0,
                    'digital'=>[
                        'total'=>number_format($total_digital_redemptions),
                        'percent'=> $total_digital_redemptions ? number_format(100*($total_digital_redemptions/$total_redemptions)) : 0
                    ],
                    'physical'=>[
                        'total'=>number_format($total_physical_redemptions),
                        'percent'=> $total_physical_redemptions ? number_format(100*($total_physical_redemptions/$total_redemptions)) : 0
                    ],
                    'upsell'=>[
                        'total'=>number_format($total_upsell_redemptions),
                        'percent'=> $total_upsell_redemptions ? number_format(100*($total_upsell_redemptions/$total_redemptions)) : 0
                    ],
                    'primary'=>[
                        'total'=>number_format($primary),
                        'percent'=> $primary ? number_format(100*($primary/$total_redemptions)) : 0
                    ],
                    'secondary'=>[
                        'total'=>number_format($secondary),
                        'percent'=> $secondary ? number_format(100*($secondary/$total_redemptions)) : 0
                    ],
                    'optins'=>number_format($optins),
                    'contest_entries'=>number_format($contest_entries),
                    'downloads'=>$download_options,
                    'warn_insufficient_upcs'=>$warn_insufficient_upcs,
                    'products'=>$product_options,
                    'upsells'=>$upsell_options,
                    'countries' => $countries,
                    'redeemed_all'=>[
                        'total'=>$total_redeemed_all_customers,
                        'percent'=> $total_redeemed_all_customers ? number_format(100*($total_redeemed_all_customers/$total_redemptions)) : 0
                    ],
                    'redeemed_some'=>[
                        'total'=>$total_redeemed_some_customers,
                        'percent'=> ($total_redeemed_some_customers&&$total_redemptions) ? number_format(100*($total_redeemed_some_customers/$total_redemptions)) : 0
                    ],
                    'redeemed_none'=>[
                        'total'=>$total_redeemed_none_customers,
                        'percent'=> ($total_redeemed_none_customers&&$total_redemptions) ? number_format(100*($total_redeemed_none_customers/$total_redemptions)) : 0
                    ],
                ],
                'transactions' => [
                    'total'=>number_format($total_transactions),
                    'venues'=>$transaction_events
                ],
                'customers' => [
                    'total'=>number_format($total_customers),
                    'venues'=>$customer_events,
                    'redeemed_none'=>[
                        'total'=> $total_redeemed_none_customers ? number_format($total_redeemed_none_customers) : 0,
                        'percent'=>$total_redeemed_none_customers ? number_format(100*($total_redeemed_none_customers/$total_customers)) : 0
                    ],
                    'redeemed_some'=>[
                        'total'=> $total_redeemed_some_customers ? number_format($total_redeemed_some_customers) : 0,
                        'percent'=>$total_redeemed_some_customers ? number_format(100*($total_redeemed_some_customers/$total_customers)) : 0
                    ],
                    'redeemed_all'=>[
                        'total'=> $total_redeemed_all_customers ? number_format($total_redeemed_all_customers) : 0,
                        'percent'=>$total_redeemed_all_customers ? number_format(100*($total_redeemed_all_customers/$total_customers)) : 0
                    ],
                ],
                'musictoday'=>[
                    'total'=>number_format($mt_redemptions),
                    'failed'=>number_format($mt_failed_redemptions),
                    //'retried'=>number_format($mt_retried_redemptions),
                    //
                    'invalid'=>number_format($mt_invalid_redemptions),
                    //'invalid_retries'=>number_format($mt_invalid_retries),
                    'invalid_notified' => number_format($mt_invalid_notified),
                    'fail_retries'=>number_format($mt_fail_retries)
                ],
                'mainstreet'=>[
                  'total'=>number_format($mainstreet_success),
                  'failed'=>number_format($mainstreet_failed),
                ],
                'digital_reporting' => $digital_reporting,
                'redemptions_over_time_data'=>$redemptions_over_time_data,
                'redemptions_over_time_categories'=>$redemptions_over_time_categories,
            ];

            # save cache only if date range is empty
            if ($date_range == '') {
                $this->redis->hSet('BundleOverview', $redisKey, json_encode($stats));
                $this->redis->hSet('BundleOverview', "bundle:$id:cached_at", time());
            }

            return $stats;
        }
        else
        {
            return $cached;
        }
    }

    public function get_reminder_stats($bundle,$selectedEmail=2) {
        #
        # Check if we need distributionId and if it has been generated yet
        #
        

        $wc = new WhatcountsAPI();

        $reminder_stats = [];

        $distributions = $wc->get_distributions_by_campaign($bundle->getCampaignId());

        if (@$distributions) {

            foreach($distributions as $distribution) {

                if(isset($distribution->Item->CampaignDistributionID)){
                    if ($distribution->Item->SelectedEmail==$selectedEmail) {
                        $distribution_id = $distribution->Item->CampaignDistributionID;
                        if ($distribution_id) {
                            $data = $wc->get_distribution_stats($distribution_id);
                            
                            //get unique clicks and opens from the db and API
                            $past = $this->get_past_unique($bundle->getId(), $selectedEmail);
                            //add unique clicks and opens from the api for today's data
                            $unique = $this->get_present_unique($bundle->getId(), $distribution_id, $past['excluded_opens'], $past['excluded_clicks']);

                            $data->unique = array(
                                'clicks'=>$unique['clicks'],
                                'opens'=>$unique['opens']
                            );
                            $reminder_stats[] = $data;
                        }
                    }
                }

            }

        }

        //print_r($reminder_stats);

        return array_reverse($reminder_stats);

    }

    public function get_redemption_graph_data($bundle_id, $start_date, $end_date) {
        $timezone = $this->get_region($bundle_id)['timezone']; //MYSQL_TIMEZONE;
        $q = $this->adapter->createStatement("SELECT all_dates.Date AS redemption_date, SUM(CASE WHEN r.bundle_id = ? THEN r.quantity ELSE 0 END) AS redemption_count FROM (SELECT DATE_ADD(curdate(), INTERVAL 2 MONTH) - INTERVAL (a.a + (10 * b.a) ) DAY as Date FROM (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7) as a CROSS JOIN (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7) as b) all_dates LEFT OUTER JOIN redemptions AS r ON r.created_at >= CONVERT_TZ(CONCAT(all_dates.Date,' 00:00:00'),'$timezone','UTC') AND r.created_at <= CONVERT_TZ(CONCAT(all_dates.Date,' 23:59:59'),'$timezone','UTC') AND r.unredeemed=0 WHERE all_dates.Date BETWEEN ? AND ? GROUP BY all_dates.Date ORDER BY all_dates.Date");
        $q->execute([$bundle_id, $start_date, $end_date]);
        $redemptions = $q->getResource()->fetchAll(\PDO::FETCH_ASSOC);
        $result = [];
        if ($redemptions) {
            foreach ($redemptions as $r) {
                $result[$r['redemption_date']] = $r['redemption_count'];
            }
        }
        return $result;
    }

    public function build_date_query($object, $date_range)
    {
        return (!empty($date_range[0])&&!empty($date_range[1])) ? "AND {$object}.created_at BETWEEN '{$date_range[0]}  00:00:00' AND '{$date_range[1]} 23:59:59'" : "";
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
