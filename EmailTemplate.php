<?php

namespace Application\Utility;

class EmailTemplate
{
    protected $file;
    protected $values = array();
    
    public function __construct($file)
    {
        $file = __DIR__.'/views/'.$file.'.html';
        if (!file_exists($file)) return "Error cant find template {$file}.";
        $this->file = $file;
    }
    
    public function set($values)
    {
        if (!is_array($values)) return "Error expecting array.";
       foreach($values as $k => $v) {
           $this->values[$k] = $v;
       }
       if (isset($values['header_logo']) && $values['header_logo']==1) {
           $this->values['header_logo'] = "<span>&nbsp;</span>";
       } else {
           $this->values['header_logo'] = file_get_contents(__DIR__.'/views/_header_logo.html');;
       }
    }

    public function render()
    {
        if (!file_exists($this->file))
        {
            return "Error loading template file ($this->file).";
        }
        $output = file_get_contents($this->file);
  
        foreach ($this->values as $key => $value) {
            $tagToReplace = "{{{$key}}}";
            $output = str_replace($tagToReplace, $value, $output);
        }
  
        return $output;
    }
    
}